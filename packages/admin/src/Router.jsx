import React, { PureComponent } from 'react';
import { Switch, BrowserRouter } from 'react-router-dom';
import { me } from "@dannlks/store/auth/authActions";
import { connect } from 'react-redux'

import routes from './config/routes';
import Loading from "./components/Loading";
import Route from "./components/Route";

@connect( state => ({ loaded: state.app.loaded }), {me})
class Router extends PureComponent {

  constructor(props) {
    super(props);
    // this.props.me();
  }


  render() {
    return (
      this.props.loaded ?
        <BrowserRouter>
          <Switch>
            { routes.map( (route, idx) => <Route key={idx} {...route} />) }
          </Switch>
        </BrowserRouter> :
        <Loading/>
    )
  }
}

export default Router
