import React, { Component } from 'react';
import { Menu, Dropdown, Button, Avatar } from 'antd';
import { connect } from 'react-redux';
import { NavLink, withRouter } from "react-router-dom";

import './Header.scss';

const mapStateToProps = (state, props) => {
  return {}
};

@withRouter
@connect(mapStateToProps)
class Header extends Component {

  _handleLogout = () => {};

  render() {

    const menu = (
      <Menu>
        <Menu.Item key="logout" onClick={this._handleLogout}>
          Logout
        </Menu.Item>
      </Menu>
    );

    return (
      <div className="Header">
        <Dropdown overlay={menu} trigger={['click']}>
          <Avatar shape="square" icon="user" />
        </Dropdown>
      </div>
    );
  }
}

export default Header
