import React from 'react';
import { Card } from 'antd';

import './Section.scss';

const Section = ({title, options, children, footer, actions}) => (
  <Card>
    <div className="Section">
      {
        !!title && <div className="Section__title">
          {title || "Needs a title"}
        </div>
      }
      {
        !!options && <div className="Section__options">
          {options || "Needs a options"}
        </div>
      }
      {
        !!actions && <div className="Section__actions">
          {actions || "Needs a actions"}
        </div>
      }
      {
        !!children && <div className="Section__body">
          {children || "Needs a children"}
        </div>
      }
      {
        !!footer && <div className="Section__footer">
          {footer || "Needs a footer"}
        </div>
      }
    </div>
  </Card>
);

export default Section;
