export default [
  {
    key: "dashboard",
    name: "Dashboard",
    icon: "dashboard",
    url: "/",
    role: "Admin",
  },
  {
    key: "categories",
    name: "Categories",
    icon: "appstore",
    url: "/categories",
    role: "Admin",
  },
  {
    key: "news",
    name: "News",
    icon: "file-text",
    url: "/news",
    role: "Entity",
  },
  {
    key: "gallery",
    name: "Gallery",
    icon: "picture",
    url: "/gallery",
    role: "Entity",
  },
  {
    key: "donors",
    name: "Donors",
    icon: "team",
    url: "/donors",
    role: "Entity",
  },
  {
    key: "entities",
    name: "Entities",
    icon: "bank",
    url: "/entities",
    role: "Admin",
  },
  {
    key: "users",
    name: "Users",
    icon: "smile",
    url: "/users",
    role: "Admin",
  },
  {
    key: "settings",
    name: "Settings",
    icon: "setting",
    url: "/settings",
    role: "Admin",
  }
]
