import React from 'react'
import Loadable from 'react-loadable'

import Loading from '../components/Loading'

const Dashboard = Loadable({
  loader: () => import('../features/dashboard/Dashboard'),
  loading: Loading,
});

const Settings = Loadable({
  loader: () => import('../features/settings/Settings'),
  loading: Loading,
});

const Users = Loadable({
  loader: () => import('../features/users/Users'),
  loading: Loading,
});

const Login = Loadable({
  loader: () => import('../features/auth/Login'),
  loading: Loading,
});

const NotFound = Loadable({
  loader: () => import('../features/_root/NotFound'),
  loading: Loading,
});

const Forbidden = Loadable({
  loader: () => import('../features/_root/Forbidden'),
  loading: Loading,
});

const AdminContainer = Loadable({
  loader: () => import('../containers/AdminContainer'),
  loading: Loading,
});

const AuthContainer = Loadable({
  loader: () => import('../containers/AuthContainer'),
  loading: Loading,
});

const DefaultContainer = Loadable({
  loader: () => import('../containers/DefaultContainer'),
  loading: Loading,
});

const adminContainer = (Component) => (
  <AdminContainer>
    <Component/>
  </AdminContainer>
);

const authContainer = (Component) => (
  <AuthContainer>
    <Component/>
  </AuthContainer>
);

const defaultContainer = (Component) => (
  <DefaultContainer>
    <Component/>
  </DefaultContainer>
);

const routes = [
  { path: '/', exact: true, component: () => adminContainer(Dashboard) },
  { path: '/settings', component: () => adminContainer(Settings) },
  { path: '/users', component: () => adminContainer(Users) },
  { path: '/login', component: () => authContainer(Login) },
  { path: '/forbidden', component: () => defaultContainer(Forbidden) },
  { component: () => defaultContainer(NotFound) }
];

export default routes;