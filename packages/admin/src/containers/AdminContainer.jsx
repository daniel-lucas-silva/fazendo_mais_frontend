import React from 'react';
import { Layout } from 'antd';

import './AdminContainer.scss';
import Header from "../components/Header";
import Sidebar from "../components/Sidebar";
import Footer from "../components/Footer";

const AdminContainer = ({children}) => {
  return (
    <Layout className="AdminContainer">
      <Layout.Sider
        className="AdminContainer__Sider"
        breakpoint="md"
        collapsedWidth="0"
        width={220}
        theme="light"
        onBreakpoint={(broken) => {  }}
        onCollapse={(collapsed, type) => {  }}
      >
        <Sidebar />
      </Layout.Sider>
      <Layout>
        <Layout.Header
          className="AdminContainer__Header"
        >
          <Header />
        </Layout.Header>
        <Layout.Content
          className="AdminContainer__Content"
        >
          {children}
        </Layout.Content>
        <Layout.Footer
          className="AdminContainer__Footer"
        >
          <Footer />
        </Layout.Footer>
      </Layout>
    </Layout>
  );
};

export default AdminContainer;
