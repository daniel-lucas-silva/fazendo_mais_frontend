import React from 'react';

import './AuthContainer.scss';

const AuthContainer = ({children}) => {
  return (
    <div className="AuthContainer">
      {children}
    </div>
  );
};

export default AuthContainer;
