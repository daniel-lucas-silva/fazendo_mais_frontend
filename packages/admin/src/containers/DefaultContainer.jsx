import React from 'react';

const DefaultContainer = ({children}) => {
  return (
    <div className="DefaultContainer">
      {children}
    </div>
  );
};

export default DefaultContainer;
