import React from 'react'
import { Button, Icon } from 'antd';
import { withRouter } from 'react-router-dom'

@withRouter
class NotFound extends React.Component {

  render() {
    return (
      <div className="forbidden">
        <div className="wrapper">
          <Icon type="stop" />
          <h1>Not Found</h1>
          <Button type="primary" icon="search" block onClick={() => this.props.history.go(-1)}>Go back</Button>
        </div>
      </div>
    )
  }
}

export default NotFound