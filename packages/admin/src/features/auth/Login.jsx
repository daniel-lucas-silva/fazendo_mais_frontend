import React, { Component } from 'react'
import { message } from 'antd'
import { Formik } from 'formik'
import { connect } from 'react-redux'
import { login } from "@dannlks/store/auth/authActions";
import { withRouter } from 'react-router-dom'

import LoginForm, { loginValidation} from "../../forms/LoginForm";
import AuthContainer from "../../containers/AuthContainer";



const mapStateToProps = (state) => {
  return {
    user: state.auth.user
  }
};

const mapActionsToProps = {
  login
};

@withRouter
@connect(mapStateToProps, mapActionsToProps)
class Login extends Component {

  _handleSubmit = (values, bag) => {
    this.props.login(values)
      .then((res) => {
        message.success(`Welcome ${res.user.username}`);
        if(res.user.entity !== 'panding') {
          this.props.history.push('/');
        }
        else {
          this.props.history.push('/profile');
        }
      })
      .catch(({ messages, ...data }) => {
        bag.setErrors({ messages, ...data });
      })
      .finally(() => {
        bag.setSubmitting(false);
      })
  };

  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <AuthContainer>
        <div className="Login">
          <div className="Login__head">
            <h1>Fazedo Mais</h1>
          </div>
          <div className="Login__form">
            <Formik
              initialValues={{
                identity: "",
                password: ""
              }}
              onSubmit={this._handleSubmit}
              validationSchema={loginValidation}
              component={LoginForm}
            />
          </div>
          <div className="Login__foot">
            <p>teste</p>
          </div>
        </div>
      </AuthContainer>
    )
  }
}

export default Login
