import React, { Component } from 'react'
import { Tabs } from 'antd';

// import SettingsGeneral from './SettingsGeneral';
// import SettingsTerms from './SettingsTerms';
// import SettingsPolicy from './SettingsPolicy';

const { TabPane } = Tabs;

class Settings extends Component {
  state = {  }
  render() {
    return (
      <div className="page">

        <div className="page__title">
          <h1>Settings</h1>
        </div>

        <div className="page__options">

        </div>

        <div className="page__actions">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry.
        </div>

        <div className="page__body">
          <Tabs type="card">
            <TabPane tab="General" key="general">
              {/*<SettingsGeneral />*/}
            </TabPane>
            <TabPane tab="Terms and Conditions" key="terms">
              {/*<SettingsTerms/>*/}
            </TabPane>
            <TabPane tab="Privacy Policy" key="policy">
              {/*<SettingsPolicy/>*/}
            </TabPane>
          </Tabs>
        </div>

      </div>
    )
  }
}

export default Settings