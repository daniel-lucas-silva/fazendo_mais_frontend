import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { message } from 'antd'
import { Formik } from 'formik'

import SettingsPolicyForm, { settingsPolicyValidation } from '@godporn/forms/SettingsPolicyForm';

@inject('settings')
@observer
class SettingsPolicy extends Component {

  constructor(props) {
    super(props)
    this.formikRef = React.createRef();
    this.data = {}
    this.state = { loaded: false }
    this.props.settings.get(true).then(settings => {
      this.data = settings
      this.setState({ loaded: true });
    })
  }

  _handleSubmit = (values, bag) => {

    const { props: { settings } } = this

    settings.save(values)
      .then((settings) => {
        this.data = settings
        message.success('Policy setting updated!');
        // this._reset()
      })
      .catch(({ data, messages }) => {
        bag.setErrors({ messages, ...data });
      })
      .finally(() => {
        bag.setSubmitting(false);
      })
  }

  _reset = () => {
    // this.props.history.push(`/settings`)
    this.formikRef.current.resetForm();
  }

  render() {
    const { state: { loaded }, _handleSubmit, formikRef, data } = this
    return (
      loaded &&
      <Formik
        ref={formikRef}
        enableReinitialize
        initialValues={{
          policy: data.policy || "",
        }}
        onSubmit={_handleSubmit}
        validationSchema={settingsPolicyValidation}
        component={SettingsPolicyForm}
      />
    )
  }
}

export default SettingsPolicy