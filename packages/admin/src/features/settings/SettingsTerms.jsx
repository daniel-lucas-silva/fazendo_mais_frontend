import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { message } from 'antd'
import { Formik } from 'formik'

import SettingsTermsForm, { settingsTermsValidation } from '@godporn/forms/SettingsTermsForm';

@inject('settings')
@observer
class SettingsTerms extends Component {

  constructor(props) {
    super(props)
    this.formikRef = React.createRef();
    this.data = {}
    this.state = { loaded: false }
    this.props.settings.get(true).then(settings => {
      this.data = settings
      this.setState({ loaded: true });
    })
  }

  _handleSubmit = (values, bag) => {

    const { props: { settings } } = this

    settings.save(values)
      .then((settings) => {
        this.data = settings
        message.success('Terms setting updated!');
        // this._reset()
      })
      .catch(({ data, messages }) => {
        bag.setErrors({ messages, ...data });
      })
      .finally(() => {
        bag.setSubmitting(false);
      })
  }

  _reset = () => {
    // this.props.history.push(`/settings`)
    this.formikRef.current.resetForm();
  }

  render() {
    const { state: { loaded }, _handleSubmit, formikRef, data } = this
    return (
      loaded &&
      <Formik
        ref={formikRef}
        enableReinitialize
        initialValues={{
          terms: data.terms || "",
        }}
        onSubmit={_handleSubmit}
        validationSchema={settingsTermsValidation}
        component={SettingsTermsForm}
      />
    )
  }
}

export default SettingsTerms