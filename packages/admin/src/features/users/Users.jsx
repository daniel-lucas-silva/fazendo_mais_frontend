import React, { Component, Fragment } from 'react'
import { Button } from 'antd'

// import SearchBar from '../../components/SearchBar'
// import UserList from './UsersList'
// import UserDrawer from './UsersDrawer'

class Users extends Component {

  userDrawer = {};
  userList = {};

  shouldComponentUpdate() {
    return false;
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    if (id) this.userDrawer.open(id);
  }

  _handleEditOpen = (id) => {
    this.userDrawer.open(id);
  }

  render() {
    return (
      <Fragment>
        <div className="page">

          <div className="page__title">
            <h1>Users</h1>
          </div>

          <div className="page__options">
            {/*<SearchBar onSearch={value => this.refs.userList.wrappedInstance.search(value)} />*/}
          </div>

          <div className="page__actions">
            <Button type="dashed" icon="plus" block onClick={() => this.userDrawer.open()}>Add new</Button>
          </div>

          <div className="page__body">
            {/*<UserList*/}
              {/*ref="userList"*/}
              {/*onEdit={this._handleEditOpen}*/}
            {/*/>*/}
          </div>

        </div>
        {/*<UserDrawer wrappedComponentRef={c => this.userDrawer = c}/>*/}
      </Fragment>
    )
  }
}

export default Users
