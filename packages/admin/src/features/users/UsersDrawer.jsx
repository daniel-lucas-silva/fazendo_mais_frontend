import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Drawer, message } from 'antd'
import { Formik } from 'formik'
import { withRouter } from 'react-router-dom'

import UsersForm, { usersValidation } from '@godporn/forms/UsersForm';

@inject('users')
@observer
@withRouter
class UserDrawer extends Component {

  constructor(props) {
    super(props)
    this.formikRef = React.createRef();
    this.data = {}
    this.state = {
      visible: false
    }
  }

  open = (id) => {

    if (id) {
      this.props.users.load(id, true)
        .then(item => {
          this.data = item;
        })
        .finally(() => {
          this.props.history.push(`/users/${id}`)
          this.setState({ visible: true })
        })
    }
    else {
      this.setState({ visible: true })
    }

  }

  _handleSubmit = (values, bag) => {

    const { data, props: { users } } = this

    if (data.id)
      users.update(values)
        .then(() => {
          message.success('User updated!');
          this._reset()
        })
        .catch(({ data, messages }) => {
          bag.setErrors({ messages, ...data });
        })
        .finally(() => {
          bag.setSubmitting(false);
        })
    else
      users.save(values)
        .then(() => {
          message.success('User saved!');
          this._reset()
        })
        .catch(({ data, messages }) => {
          bag.setErrors({ messages, ...data });
        })
        .finally(() => {
          bag.setSubmitting(false);
        })
  }

  _reset = () => {
    this.data = {}
    this.props.history.push(`/users`)
    this.formikRef.current.resetForm();
    this.setState({ visible: false })
  }

  render() {
    const { state: { visible }, _reset, _handleSubmit, formikRef, data } = this
    return (
      <Drawer
        title="User"
        visible={visible}
        onClose={_reset}
        footer={null}
        width={400}
        placement="right"
        maskClosable={false}
        style={{
          height: 'calc(100% - 55px)',
          overflow: 'auto',
          paddingBottom: 53,
        }}
      >
        {
          visible &&
          <Formik
            ref={formikRef}
            enableReinitialize
            initialValues={{
              id: data.id || null,
              username: data.username || "",
              email: data.email || "",

              level: data.level || "",
            }}
            onSubmit={_handleSubmit}
            validationSchema={usersValidation}
            component={UsersForm}
          />
        }
      </Drawer>
    )
  }
}

export default UserDrawer
