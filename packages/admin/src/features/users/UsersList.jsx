import React, { Component } from 'react'
import { Table, Button, Menu, Dropdown, Icon } from 'antd'
import { inject, observer } from 'mobx-react'

@inject('users')
@observer
class UserList extends Component {

  componentDidMount() {
    this.props.users.fetch()
  }

  columns = [
    {
      title: 'Username',
      dataIndex: 'username',
      render: (username, data) => <a href="#void" onClick={(e) => { e.preventDefault(); this._handleEdit(data.id); }}>{username}</a>,
    },
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: 'Level',
      dataIndex: 'level',
    },
    {
      title: '',
      key: 'action',
      width: '10%',
      render: (text, record) => (
        <Dropdown overlay={this.menu(record.id)}>
          <Button>
            <Icon type="ellipsis" theme="outlined" />
          </Button>
        </Dropdown>
      ),
    }
  ];

  menu = (id) => (
    <Menu>
      <Menu.Item key="1" onClick={() => this._handleDelete(id)}>Delete</Menu.Item>
      <Menu.Item key="2" onClick={() => this._handleEdit(id)}>Edit</Menu.Item>
    </Menu>
  );

  _handleEdit = (id) => this.props.onEdit(id)

  _handleDelete = (id) => {
    this.props.users.deleteUser(id)
  }

  _handleChange = (page, size) => {
    const { users } = this.props

    if (this.searching)
      users.searchUsers(`${this.searching}?offset=${(page - 1) * size}`)
    else
      users.fetch(`offset=${(page - 1) * size}`)
  }

  search = (text) => {
    const { users } = this.props

    if (text !== "") {
      this.searching = text;
      users.searchUsers(text)
    }
    else
      users.fetch()
  }

  render() {
    const { users, page, size, total, loading } = this.props.users

    return (
      <Table
        columns={this.columns}
        rowKey={record => record.id}
        dataSource={users}
        pagination={{
          showQuickJumper: true,
          total: total,
          pageSize: size,
          current: page,
          // showSizeChanger: true,
          onChange: (page, pageSize) => this._handleChange(page, pageSize),
          // onShowSizeChange: (current, size) => console.log(current, size)
        }}
        loading={loading}
        // onChange={this._handleChange}
        onRow={(record) => {
          return {
            // onClick: () => this._handleEdit(record.id),
          };
        }}
      />
    )
  }
}

export default UserList