import React from 'react'
import { Button, Alert } from 'antd'
import * as Yup from 'yup'

import TextInput from './components/TextInput'
import FileInput from './components/FileInput'
import TextArea from './components/TextArea'

const newsValidation = () => {
  return Yup.object().shape({
    title: Yup.string().required(),
    content: Yup.string().required(),
    // thumbnail: Yup.string().required(),
  })
};

const NewsForm = (props) => {

  const {
    values,
    handleSubmit,
    handleChange,
    errors,
    touched,
    handleBlur,
    // isValid,
    setFieldValue,
    isSubmitting
  } = props;

  return (
    <form onSubmit={handleSubmit} className="newsForm">
      <div className="newsForm__errors">
        {
          !!errors.messages && <Alert
            message="Error"
            description={errors.messages}
            type="error"
            closable
          />
        }
      </div>
      <div className="newsForm__title">
        <TextInput
          name="title"
          type="text"
          label="Title"
          value={values.title}
          onChange={handleChange}
          onBlur={handleBlur}
          error={touched.title && errors.title}
        />
      </div>

      <div className="newsForm__desc">
        <TextArea
          name="content"
          label="Content"
          value={values.content}
          onChange={handleChange}
          onBlur={handleBlur}
          error={touched.content && errors.content}
          autosize={{ minRows: 4, maxRows: 15 }}
        />
      </div>

      <div className="newsForm__thumb">
        <FileInput
          name="thumbnail"
          label="Thumbnail"
          width={300}
          height={300}
          value={values.thumbnail}
          setFieldValue={setFieldValue}
          error={touched.thumbnail && errors.thumbnail}
        />
      </div>

      <div className="newsForm__submit">
        <Button
          size="large"
          type="primary"
          htmlType="submit"
          block
          loading={isSubmitting}
        >
          Save
        </Button>
      </div>

    </form>
  )

}

export {
  newsValidation
}

export default NewsForm