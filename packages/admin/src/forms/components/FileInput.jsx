import React, { Component, Fragment } from 'react';
import { Icon, Upload } from 'antd';
import ImgCrop from 'antd-img-crop';
import classnames from 'classnames';

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img)
}

class FileInput extends Component {

  state = {
    imageData: null,
    loading: false,
  };


  componentWillMount() {
    if (this.props.value) {
      this.setState({
        imageData: this.props.value
      });
    }
  }

  _handleChange = value => {
    this.props.setFieldValue(this.props.name, value)
  };

  _handleBlur = value => {
    this.props.onBlur(this.props.name)
  };

  _handleImage = data => {
    this.setState({ loading: true });
    getBase64(data.file, imageData => {
      this.setState({
        imageData,
        loading: false,
      });
      this._handleChange(imageData);
    })
  };

  render() {
    const { label, info, error, name, width = 100, height = 100, ...rest } = this.props;
    const { imageData, loading } = this.state;

    const uploadButton = (
      <div>
        <Icon type={ loading ? 'loading' : 'plus' } />
        <div className="ant-upload-text">Upload</div>
      </div>
    );

    return (
      <Fragment>
        {label && <label className="field__label">{label}</label>}

        <ImgCrop
          modalTitle="Crop Image"
          width={width}
          height={height}
        >
          <Upload
            {...rest}
            name={name}
            listType="picture-card"
            className={classnames('field__input', { 'field__input-error': error })}
            showUploadList={false}
            customRequest={this._handleImage}
          >
            { imageData ? <img src={imageData} alt="avatar" /> : uploadButton }
          </Upload>
        </ImgCrop>

        {(error && <span className="field__error">{error}</span>) || (info && <span className="field__info">{info}</span>)}
      </Fragment>
    );
  }
}

export default FileInput;
