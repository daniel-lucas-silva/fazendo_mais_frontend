import React, { Fragment } from 'react';
import { Input } from 'antd';
import classnames from 'classnames';

const TextArea = ({ label, type, placeholder, info, error, icon, autosize = { minRows: 2, maxRows: 6 }, ...input }) => (
  <Fragment>
    {label && <label className="field__label">{label}</label>}
    <Input.TextArea
      className={classnames('field__input', { 'field__input-error': error })}
      type={type}
      placeholder={placeholder}
      autosize={autosize}
      {...input}
    />
    {(error && <span className="field__error">{error}</span>) || (info && <span className="field__info">{info}</span>)}
  </Fragment>
)

export default TextArea
