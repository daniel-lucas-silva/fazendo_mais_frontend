import React, { Fragment } from 'react';
import { Input, Icon } from 'antd';
import classnames from 'classnames';

const TextInput = ({ label, type, placeholder, info, error, icon, ...input }) => (
  <Fragment>
    {label && <label className="field__label">{label}</label>}
    <Input
      className={classnames('field__input', {'field__input-error': error})}
      type={type}
      placeholder={placeholder}
      prefix={icon && <Icon type={icon} style={{ color: 'rgba(0,0,0,.25)' }} />}
      suffix={error && <Icon type="exclamation-circle" theme="filled" style={{ color: 'red' }} />}
      {...input}
    />
    {(error && <span className="field__error">{error}</span>) || (info && <span className="field__info">{info}</span>)}
  </Fragment>
)

export default TextInput
