const rewireYarnWorkspaces = require('react-app-rewire-yarn-workspaces');
const { injectBabelPlugin } = require('react-app-rewired');

module.exports = function override(config, env) {

  config = rewireYarnWorkspaces(config, env);

  config = injectBabelPlugin(
    [
      "@babel/plugin-proposal-decorators",
      {
        legacy: true
      }
    ],
    config
  );

  config = injectBabelPlugin(
    [
      'import',
      {
        libraryName: 'antd',
        libraryDirectory: 'lib',
        style: 'css'
      },
      "antd"
    ],
    config,
  );

  config = injectBabelPlugin(
    [
      'import',
      {
        libraryName: "lodash",
        libraryDirectory: "",
        camel2DashComponentName: false
      },
      "lodash"
    ],
    config,
  );

  return config;
};
