import React, { Fragment } from 'react';

const Footer = () => {
  return (
    <Fragment>
      FazendoMais ©2018 Todos os direitos reservados.
    </Fragment>
  );
};

export default Footer;
