import React, { Component } from 'react';
import { Menu, Dropdown, Avatar } from 'antd';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { logout } from "@dannlks/store/auth/authActions";

import './Header.scss';

const mapStateToProps = (state, props) => {
  return {}
};

const mapActionsToProps = {
  logout
};

@withRouter
@connect(mapStateToProps, mapActionsToProps)
class Header extends Component {

  _handleLogout = () => {
    this.props.logout();
    this.props.history.push('/login')
  };

  render() {

    const menu = (
      <Menu>
        <Menu.Item key="logout" onClick={this._handleLogout}>
          Logout
        </Menu.Item>
      </Menu>
    );

    return (
      <div className="Header">
        <Dropdown overlay={menu} trigger={['click']}>
          <Avatar shape="square" icon="user" />
        </Dropdown>
      </div>
    );
  }
}

export default Header
