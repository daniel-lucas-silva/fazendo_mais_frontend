import React, { PureComponent } from 'react'
import { Route as ReactRoute, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
  return {
    user: state.auth.user
  }
};

@withRouter
@connect(mapStateToProps)
class Route extends PureComponent {

  componentWillMount() {
    if(!localStorage.getItem('token')) {
      if(this.props.role === 'Entity')
        this.props.history.push('/login')
    }

    if(localStorage.getItem('token')) {
      if(this.props.role === 'Guest')
        this.props.history.push('/')
    }
  }

  _authGuard() {

    if(!!this.props.user) {
      if(this.props.role === 'Guest') {
          this.props.history.push('/');
      }
      if(this.props.user.level !== 'Entity') {
        this.props.history.push('/forbidden');
      }
      if(this.props.user.entity === 'pending' && this.props.location.pathname !== '/profile') {
        this.props.history.push('/profile')
      }
    }
  }

  render() {
    const { path, exact, component } = this.props;

    this._authGuard();
    window.scrollTo(0, 0);

    return (
      <ReactRoute path={path} exact={exact} component={component} />
    )
  }
}

export default Route;
