import React, { Component } from 'react';
import { NavLink, withRouter } from "react-router-dom";
import { Menu, Icon } from 'antd';
import { connect } from 'react-redux';

import menu from '../config/menu';
import './Sidebar.scss';

@withRouter
@connect( state => ({ auth: state.auth }))
class Sidebar extends Component {

  state = {
    role: ""
  };

  componentWillReceiveProps(props) {
    if(!!props.auth.user.level) {
      this.setState({ role: props.auth.user.level})
    }
  }

  render() {

    let current = this.props.location.pathname.split('/');
    current.splice(0, 1);

    return (
      <div className="Sidebar">
        <img className="logo" src="/logo.png" alt="Fazendo Mais" />

        <Menu mode="inline" selectedKeys={!!current[0] ? current : ['dashboard']}>
          {
            menu
              .map(item => {
                return (
                  <Menu.Item key={item.key}>
                    <NavLink to={item.url}>
                      {item.icon && <Icon type={item.icon} />}
                      <span className="nav-text">{item.name}</span>
                    </NavLink>
                  </Menu.Item>
                )
              })
          }
        </Menu>
      </div>
    );
  }
}

Sidebar.propTypes = {};

export default Sidebar
