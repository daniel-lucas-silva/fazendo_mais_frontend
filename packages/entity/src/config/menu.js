export default [
  {
    key: "dashboard",
    name: "Dashboard",
    icon: "dashboard",
    url: "/",
  },
  {
    key: "news",
    name: "News",
    icon: "file-text",
    url: "/news",
  },
  {
    key: "balance",
    name: "Balance",
    icon: "dollar",
    url: "/balance",
  },
  {
    key: "reports",
    name: "Reports",
    icon: "file-protect",
    url: "/reports",
  },
  {
    key: "gallery",
    name: "Gallery",
    icon: "picture",
    url: "/gallery",
  },
  {
    key: "donors",
    name: "Donors",
    icon: "team",
    url: "/donors",
  }
]
