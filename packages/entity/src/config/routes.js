import React from 'react'
import Loadable from 'react-loadable'

import Loading from '../components/Loading'

const Profile = Loadable({
  loader: () => import('../features/profile/Profile'),
  loading: Loading,
});

const Dashboard = Loadable({
  loader: () => import('../features/dashboard/Dashboard'),
  loading: Loading,
});

const News = Loadable({
  loader: () => import('../features/news/News'),
  loading: Loading,
});

const Balance = Loadable({
  loader: () => import('../features/balance/Balance'),
  loading: Loading,
});

const Reports = Loadable({
  loader: () => import('../features/reports/Reports'),
  loading: Loading,
});

const Gallery = Loadable({
  loader: () => import('../features/gallery/Gallery'),
  loading: Loading,
});

const Login = Loadable({
  loader: () => import('../features/auth/Login'),
  loading: Loading,
});

const Register = Loadable({
  loader: () => import('../features/auth/Register'),
  loading: Loading,
});

const NotFound = Loadable({
  loader: () => import('../features/_root/NotFound'),
  loading: Loading,
});

const Forbidden = Loadable({
  loader: () => import('../features/_root/Forbidden'),
  loading: Loading,
});

const AdminContainer = Loadable({
  loader: () => import('../containers/AdminContainer'),
  loading: Loading,
});

const AuthContainer = Loadable({
  loader: () => import('../containers/AuthContainer'),
  loading: Loading,
});

const DefaultContainer = Loadable({
  loader: () => import('../containers/DefaultContainer'),
  loading: Loading,
});

const adminContainer = (Component) => (
  <AdminContainer>
    <Component/>
  </AdminContainer>
);

const authContainer = (Component) => (
  <AuthContainer>
    <Component/>
  </AuthContainer>
);

const defaultContainer = (Component) => (
  <DefaultContainer>
    <Component/>
  </DefaultContainer>
);

const routes = [
  { path: '/', exact: true, component: () => adminContainer(Dashboard) },
  { path: '/news/:id?', component: () => adminContainer(News) },
  { path: '/balance/:id?', component: () => adminContainer(Balance) },
  { path: '/reports/:id?', component: () => adminContainer(Reports) },
  { path: '/gallery/:id?', component: () => adminContainer(Gallery) },
  { path: '/profile', component: () => adminContainer(Profile) },
  { path: '/login', component: () => authContainer(Login) },
  { path: '/register', component: () => authContainer(Register) },
  { path: '/forbidden', component: () => defaultContainer(Forbidden) },
  { component: () => defaultContainer(NotFound) }
];

export default routes;