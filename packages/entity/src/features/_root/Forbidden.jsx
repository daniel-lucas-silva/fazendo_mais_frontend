import React from 'react'
import { Button, Icon } from 'antd';

class Forbidden extends React.Component {

  render() {
    return (
      <div className="forbidden">
        <div className="wrapper">
          <Icon type="stop" />
          <h1>Forbidden</h1>
        </div>
      </div>
    )
  }
}

export default Forbidden