import React, { Component } from 'react'
import { message } from 'antd'
import { Formik } from 'formik'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { register } from "@dannlks/store/auth/authActions";
import RegisterForm, { registerValidation } from "@dannlks/forms/RegisterForm";

import AuthContainer from "../../containers/AuthContainer";

const mapStateToProps = (state) => {
  return {
    user: state.auth.user
  }
};

const mapActionsToProps = {
  register
};

@connect(mapStateToProps, mapActionsToProps)
class Register extends Component {

  componentDidMount() {
    if(!!this.props.user) {
      if(this.props.user.entity === 'pending'){
        this.props.history.push('/new/entity');
      }
      else {
        this.props.history.push('/');
      }
    }
  }

  _handleSubmit = (values, bag) => {
    this.props.register(values)
      .then(() => {
        this.props.history.replace('/');
        message.success('Welcome');
      })
      .catch(({ data, messages }) => {
        bag.setErrors({ messages, ...data });
      })
      .finally(() => {
        bag.setSubmitting(false);
      })
  }

  render() {
    return (
      <AuthContainer>
        <div className="Register">
          <div className="Register__head">
            <h1>FazendoMais</h1>
          </div>
          <div className="Register__form">
            <Formik
              initialValues={{
                username: "",
                email: "",
                password: "",
                confirmPassword: "",
                is_entity: true,
                agreed: false,
              }}
              onSubmit={this._handleSubmit}
              validationSchema={registerValidation}
              component={RegisterForm}
            />
          </div>
          <div className="Register__foot">
            <p>Já tem uma conta? <NavLink to="/login">Entrar</NavLink></p>
          </div>
        </div>
      </AuthContainer>
    )
  }
}

export default Register
