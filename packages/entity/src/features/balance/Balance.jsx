import React, { Component, Fragment } from 'react'

import BalanceTable from './BalanceTable'
import BalanceModal from './BalanceModal'

class Balance extends Component {

  _handleEditOpen = (id) => {
    this.modal.open(id);
  };

  _handleTableUpdate = () => {
    this.table.load();
  };

  render() {
    return (
      <Fragment>
        <BalanceTable wrappedComponentRef={ref => this.table = ref} onOpenModal={id => this._handleEditOpen(id) } />
        <BalanceModal wrappedComponentRef={ref => this.modal = ref} onComplete={() => this._handleTableUpdate() } />
      </Fragment>
    )
  }
}

export default Balance
