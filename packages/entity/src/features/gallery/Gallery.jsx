import React, { Component, Fragment } from 'react'

import GalleryTable from './GalleryTable'
import GalleryModal from './GalleryModal'

class Gallery extends Component {

  _handleEditOpen = (id) => {
    this.modal.open(id);
  };

  _handleTableUpdate = () => {
    this.table.load();
  };

  render() {
    return (
      <Fragment>
        <GalleryTable wrappedComponentRef={ref => this.table = ref} onOpenModal={id => this._handleEditOpen(id) } />
        <GalleryModal wrappedComponentRef={ref => this.modal = ref} onComplete={() => this._handleTableUpdate() } />
      </Fragment>
    )
  }
}

export default Gallery
