import React, {Component} from 'react';
import { Modal, message, Drawer } from 'antd';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import { withRouter } from 'react-router-dom';
// import GalleryForm, { reportValidation } from "@dannlks/forms/GalleryForm";
import { getGallery, saveGallery, updateGallery } from "@dannlks/store/galleries/galleriesActions";

const mapActionsToProps = {
  getGallery,
  saveGallery,
  updateGallery
};

@withRouter
@connect( null, mapActionsToProps, null, { forwardRef: true })
class GalleryModal extends Component {

  constructor(props) {
    super(props);
    this.data = {};
    this.state = {
      visible: false,
      mounted: false
    };

    const { id } = this.props.match.params;
    if (id) this.open(id);
  }

  open = (id) => {
    if(id) {
      this.props.getGallery(id)
        .then((data) => {
          this.data = data;
        })
        .finally(() => {
          this.props.history.push(`/gallery/${id}`);
          this.setState({ visible: true, mounted: true })
        })
    }
    else {
      this.setState({ visible: true, mounted: true })
    }
  };

  _handleSubmit = (values, bag) => {
    const { data, props: { saveGallery, updateGallery } } = this;

    if (data.id)
      updateGallery(data.id, values)
        .then(() => {
          setTimeout(() => message.success('Gallery updated!'), 600)
          this._reset()
        })
        .catch(({ data, messages }) => {
          bag.setErrors({ messages, ...data });
        })
        .finally(() => {
          bag.setSubmitting(false);
          this.props.onComplete();
        });
    else
      saveGallery(values)
        .then(() => {
          setTimeout(() => message.success('Gallery saved!'), 600)
          this._reset()
        })
        .catch((data) => {
          console.log('Error ', data)
          // bag.setErrors({ messages, ...data });
        })
        .finally(() => {
          bag.setSubmitting(false);
          this.props.onComplete();
        })
  };

  _reset = () => {
    this.setState({ visible: false });
    this.props.history.goBack();
    this.formikRef.resetForm();
    this.data = {};
  };

  render() {
    const { state: { visible, mounted }, _reset, _handleSubmit, data } = this;
    return (
      <Drawer
        title="Gallery"
        destroyOnClose
        visible={visible}
        onClose={_reset}
        width={780}
        placement="right"
        maskClosable={false}
        style={{
          height: 'calc(100% - 55px)',
          overflow: 'auto',
        }}
      >

      </Drawer>
    );
  }
}

GalleryModal.propTypes = {};

export default GalleryModal;
