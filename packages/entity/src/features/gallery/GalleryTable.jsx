import React, {Component} from 'react';
import { Table, Input, Button, Dropdown, Icon, Menu, Modal } from 'antd';
import {connect} from "react-redux";
import { withRouter } from 'react-router-dom';
import queryString from 'query-string';
import { fetchGalleries, deleteGallery } from "@dannlks/store/galleries/galleriesActions";

import Section from "../../components/Section";

const confirm = Modal.confirm;

const mapStateToProps = (state, props) => {
  return {
    gallery: state.gallery
  }
};

const mapActionsToProps = {
  fetchGalleries,
  deleteGallery
};

@withRouter
@connect(mapStateToProps, mapActionsToProps, null, { forwardRef: true })
class GalleryTable extends Component {

  state = {
    selectedRowKeys: []
  };

  constructor(props) {
    super(props);

    this.query = queryString.parse(this.props.location.search);
    this.load(this.props.location.search);
  }


  load = () => {
    const { q, ...rest} = this.query;
    const search = !!q ? `/search/${q}` : '';
    const params = queryString.stringify(rest);
    this.props.fetchGalleries(`${search}?${params}`);
    this.props.history.push(`/gallery?${queryString.stringify({q, ...rest})}`)
  };

  _change = (pagination, filter, sorter) => {
    this.query.page =  pagination.current;
    this.query.limit = pagination.pageSize;
    if(Object.keys(sorter).length) {
      this.query.sort = sorter.field;
      this.query.order = sorter.order === 'ascend' ? 'asc' : 'desc'
    } else {
      delete this.query['sort'];
      delete this.query['order'];
    }
    this.load();
  };

  _handleSize = (size) => {
    const query = this.query;
    query.limit = size;
    this.load();
  };

  _handleDelete = async (id) => {
    const { selectedRowKeys } = this.state;

    confirm({
      title: `Do you want to delete ${id ? 'this item':'these items'}?`,
      content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
      onOk: async () => {
        if(id) {
          await this.props.deleteGallery(id);
          await this.load()
        }
        else {
          if (selectedRowKeys.length !== 0) {
            for (const key of Object.keys(selectedRowKeys)) {
              await this.props.deleteGallery(selectedRowKeys[key])
              // console.log(selectedRowKeys[key])
            }
            await this.load();
            this.setState({selectedRowKeys: []})
          }
        }
      },
      onCancel() {},
    });


  };

  render() {
    const { page, size, total, rows } = this.props.gallery;
    return (
      <Section
        title={<h2>Gallery</h2>}
        options={
          <div style={{display: 'flex'}}>

          </div>
        }
      >

      </Section>
    );
  }
}

export default GalleryTable;
