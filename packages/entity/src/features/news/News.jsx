import React, { Component, Fragment } from 'react'

import NewsTable from './NewsTable'
import NewsModal from './NewsModal'

class News extends Component {

  _handleEditOpen = (id) => {
    this.modal.open(id);
  };

  _handleTableUpdate = () => {
    this.table.load();
  };

  render() {
    return (
      <Fragment>
        <NewsTable wrappedComponentRef={ref => this.table = ref} onOpenModal={id => this._handleEditOpen(id) } />
        <NewsModal wrappedComponentRef={ref => this.modal = ref} onComplete={() => this._handleTableUpdate() } />
      </Fragment>
    )
  }
}

export default News
