import React, {Component} from 'react';
import { Modal, message, Drawer } from 'antd';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import { withRouter } from 'react-router-dom';
import ReportForm, { reportValidation } from "@dannlks/forms/ReportForm";
import { getReport, saveReport, updateReport } from "@dannlks/store/reports/reportsActions";

const mapActionsToProps = {
  getReport,
  saveReport,
  updateReport
};

@withRouter
@connect( null, mapActionsToProps, null, { forwardRef: true })
class ReportModal extends Component {

  constructor(props) {
    super(props);
    this.data = {};
    this.state = {
      visible: false,
      mounted: false
    };

    const { id } = this.props.match.params;
    if (id) this.open(id);
  }

  open = (id) => {
    if(id) {
      this.props.getReport(id)
        .then((data) => {
          this.data = data;
        })
        .finally(() => {
          this.props.history.push(`/reports/${id}`);
          this.setState({ visible: true, mounted: true })
        })
    }
    else {
      this.setState({ visible: true, mounted: true })
    }
  };

  _handleSubmit = (values, bag) => {
    const { data, props: { saveReport, updateReport } } = this;

    if (data.id)
      updateReport(data.id, values)
        .then(() => {
          setTimeout(() => message.success('Report updated!'), 600)
          this._reset()
        })
        .catch(({ data, messages }) => {
          bag.setErrors({ messages, ...data });
        })
        .finally(() => {
          bag.setSubmitting(false);
          this.props.onComplete();
        });
    else
      saveReport(values)
        .then(() => {
          setTimeout(() => message.success('Report saved!'), 600)
          this._reset()
        })
        .catch((data) => {
          console.log('Error ', data)
          // bag.setErrors({ messages, ...data });
        })
        .finally(() => {
          bag.setSubmitting(false);
          this.props.onComplete();
        })
  };

  _reset = () => {
    this.setState({ visible: false });
    this.props.history.goBack();
    this.formikRef.resetForm();
    this.data = {};
  };

  render() {
    const { state: { visible, mounted }, _reset, _handleSubmit, data } = this;
    return (
      <Drawer
        title="Report"
        destroyOnClose
        visible={visible}
        onClose={_reset}
        width={780}
        placement="right"
        maskClosable={false}
        style={{
          height: 'calc(100% - 55px)',
          overflow: 'auto',
        }}
      >
        <Formik
          ref={ ref => this.formikRef = ref}
          enableReinitialize
          initialValues={{
            id: data.id || null,
            title: data.title || "",
            content: data.content || "",
            thumbnail: data.thumbnail || ""
          }}
          onSubmit={_handleSubmit}
          validationSchema={reportValidation}
          component={ReportForm}
        />
      </Drawer>
    );
  }
}

ReportModal.propTypes = {};

export default ReportModal;
