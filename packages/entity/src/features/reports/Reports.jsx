import React, { Component, Fragment } from 'react'

import ReportsTable from './ReportsTable'
import ReportModal from './ReportModal'

class Reports extends Component {

  _handleEditOpen = (id) => {
    this.modal.open(id);
  };

  _handleTableUpdate = () => {
    this.table.load();
  };

  render() {
    return (
      <Fragment>
        <ReportsTable wrappedComponentRef={ref => this.table = ref} onOpenModal={id => this._handleEditOpen(id) } />
        <ReportModal wrappedComponentRef={ref => this.modal = ref} onComplete={() => this._handleTableUpdate() } />
      </Fragment>
    )
  }
}

export default Reports
