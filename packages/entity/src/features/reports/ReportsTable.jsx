import React, {Component} from 'react';
import { Table, Input, Button, Dropdown, Icon, Menu, Modal } from 'antd';
import {connect} from "react-redux";
import { withRouter } from 'react-router-dom';
import queryString from 'query-string';
import { fetchReports, deleteReport } from "@dannlks/store/reports/reportsActions";

import Section from "../../components/Section";

const confirm = Modal.confirm;
const Search = Input.Search;
const ButtonGroup = Button.Group;

const mapStateToProps = (state, props) => {
  return {
    reports: state.reports
  }
};

const mapActionsToProps = {
  fetchReports,
  deleteReport
};

@withRouter
@connect(mapStateToProps, mapActionsToProps, null, { forwardRef: true })
class ReportsTable extends Component {

  state = {
    selectedRowKeys: []
  };

  constructor(props) {
    super(props);

    this.query = queryString.parse(this.props.location.search);

    this.columns = [
      {
        title: 'Title',
        dataIndex: 'title',
        sorter: true,
        render: (title, data) => <a href="#void" onClick={(e) => { e.preventDefault(); this.props.onOpenModal(data.id); } }>{title}</a>
      },
      {
        title: '',
        key: 'action',
        width: '10%',
        render: (text, record) => (
          <Dropdown overlay={this.actions(record.id)} trigger={['click']}>
            <Button type="primary" shape="circle" ghost size="small">
              <Icon type="ellipsis" theme="outlined" />
            </Button>
          </Dropdown>
        ),
      }
    ];

    this.load(this.props.location.search);
  }

  actions = (id) => (
    <Menu>
      <Menu.Item key="1" onClick={() => this._handleDelete(id)}>Delete</Menu.Item>
      <Menu.Item key="2" onClick={() => this.props.onOpenModal(id)}>Edit</Menu.Item>
    </Menu>
  );

  load = () => {
    const { q, ...rest} = this.query;
    const search = !!q ? `/search/${q}` : '';
    const params = queryString.stringify(rest);
    this.props.fetchReports(`${search}?${params}`);
    this.props.history.push(`/reports?${queryString.stringify({q, ...rest})}`)
  };

  _change = (pagination, filter, sorter) => {
    this.query.page =  pagination.current;
    this.query.limit = pagination.pageSize;
    if(Object.keys(sorter).length) {
      this.query.sort = sorter.field;
      this.query.order = sorter.order === 'ascend' ? 'asc' : 'desc'
    } else {
      delete this.query['sort'];
      delete this.query['order'];
    }
    this.load();
  };

  _handleSize = (size) => {
    const query = this.query;
    query.limit = size;
    this.load();
  };

  _handleSearch = (term) => {
    if (!!term) {
      this.term = term;
      term = term.replace(/[^a-z0-9 ]+/gi, '%%').toLowerCase();
      const resetQuery = this.query;
      delete resetQuery['q'];
      delete resetQuery['page'];
      this.query = { q: term, ...resetQuery }
    } else {
      const resetQuery = this.query;
      delete resetQuery['page'];
      delete resetQuery['q'];
      this.query = {...resetQuery};
    }
    this.load();
  };

  onSelectChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  };

  _handleDelete = async (id) => {
    const { selectedRowKeys } = this.state;

    confirm({
      title: `Do you want to delete ${id ? 'this item':'these items'}?`,
      content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
      onOk: async () => {
        if(id) {
          await this.props.deleteReport(id);
          await this.load()
        }
        else {
          if (selectedRowKeys.length !== 0) {
            for (const key of Object.keys(selectedRowKeys)) {
              await this.props.deleteReport(selectedRowKeys[key])
              // console.log(selectedRowKeys[key])
            }
            await this.load();
            this.setState({selectedRowKeys: []})
          }
        }
      },
      onCancel() {},
    });


  };

  render() {
    const { page, size, total, rows } = this.props.reports;
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
    };
    return (
      <Section
        title={<h2>Report</h2>}
        options={
          <div style={{display: 'flex'}}>
            <ButtonGroup>
              <Button type="primary" icon="plus" onClick={() => this.props.onOpenModal()}>Novo</Button>
              {/*<Button icon="edit" disabled>Editar</Button>*/}
              <Button type="danger" icon="delete" onClick={() => this._handleDelete()} disabled={selectedRowKeys.length <= 0}>Deletar</Button>
            </ButtonGroup>
            <Search
              placeholder="Buscar..."
              defaultValue={this.query.q || null}
              onSearch={value => this._handleSearch(value)}
              style={{ width: 250, marginLeft: 10 }}
            />
          </div>
        }
      >
        <Table
          rowSelection={rowSelection}
          rowKey={record => record.id}
          columns={this.columns}
          dataSource={rows}
          onChange={this._change}
          size="middle"
          expandedRowRender={record => <p style={{ margin: 0 }}>{record.description}</p>}
          pagination={{
            showQuickJumper: true,
            total: total,
            pageSize: size,
            current: page,
            showSizeChanger: true,
            size: "middle",
            onShowSizeChange: (current, size) => this._handleSize(size),
            pageSizeOptions: ['1', '5', '10', '20', '50']
          }}
        />
      </Section>
    );
  }
}

export default ReportsTable;
