import React from 'react';
import ReactDOM from 'react-dom';
import promiseFinally from 'promise.prototype.finally'
import {setToken} from "@dannlks/store/app/appActions";
import { Provider } from 'react-redux';
import store from '@dannlks/store';

import * as serviceWorker from './serviceWorker';
import Router from './Router';
import './index.scss';

if (localStorage.token) {
  setToken(localStorage.token);
}

promiseFinally.shim();

const App = () => (
  <Provider store={store}>
    <Router/>
  </Provider>
);


ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.register();
