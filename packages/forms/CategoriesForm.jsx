import React from 'react'
import { Button, Alert } from 'antd'
import * as Yup from 'yup'

import TextInput from './components/TextInput'
import FileInput from './components/FileInput'
import TextArea from './components/TextArea'

const categoriesValidation = () => {
  return Yup.object().shape({
    title: Yup.string().required(),
    description: Yup.string().required(),
    // thumbnail: Yup.string().required(),
  })
};

const CategoriesForm = (props) => {

  const {
    values,
    handleSubmit,
    handleChange,
    errors,
    touched,
    handleBlur,
    // isValid,
    setFieldValue,
    isSubmitting
  } = props;

  return (
    <form onSubmit={handleSubmit} className="categoriesForm">
      <div className="categoriesForm__errors">
        {
          !!errors.messages && <Alert
            message="Error"
            description={errors.messages}
            type="error"
            closable
          />
        }
      </div>
      <div className="categoriesForm__title">
        <TextInput
          name="title"
          type="text"
          label="Title"
          value={values.title}
          onChange={handleChange}
          onBlur={handleBlur}
          error={touched.title && errors.title}
        />
      </div>

      <div className="categoriesForm__desc">
        <TextArea
          name="description"
          label="Description"
          value={values.description}
          onChange={handleChange}
          onBlur={handleBlur}
          error={touched.description && errors.description}
          autosize={{ minRows: 4, maxRows: 15 }}
        />
      </div>

      <div className="categoriesForm__thumb">
        <FileInput
          name="thumbnail"
          label="Thumbnail"
          width={297}
          height={163}
          value={values.thumbnail}
          setFieldValue={setFieldValue}
          error={touched.thumbnail && errors.thumbnail}
        />
      </div>

      <div className="categoriesForm__submit">
        <Button
          size="large"
          type="primary"
          htmlType="submit"
          block
          loading={isSubmitting}
        >
          Save
        </Button>
      </div>

    </form>
  )

}

export {
  categoriesValidation
}

export default CategoriesForm