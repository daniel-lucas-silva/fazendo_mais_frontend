import React, { Fragment } from 'react'
import { Button, Alert } from 'antd'
import * as Yup from 'yup'

import TextInput from './components/TextInput'

const loginValidation = () => (
  Yup.object().shape({
    identity: Yup.string().required('Please enter your username or email address.'),
    password: Yup.string().required('Please enter your password.'),
  })
)

const LoginForm = (props) => {

  const {
    values,
    handleSubmit,
    handleChange,
    errors,
    touched,
    handleBlur,
    // isValid,
    isSubmitting
  } = props;

  return (
    <Fragment>
      <form onSubmit={handleSubmit} className="LoginForm">
        <div className="LoginForm__errors">
          {
            !!errors.messages && <Alert
              message="Error"
              description={errors.messages}
              type="error"
              closable
            />
          }
        </div>
        <div className="LoginForm__input">
          <TextInput
            name="identity"
            type="text"
            icon="user"
            label="Username/Email"
            placeholder="Type your username"
            value={values.identity}
            onChange={handleChange}
            onBlur={handleBlur}
            error={touched.identity && errors.identity}
          />
        </div>
        <div className="LoginForm__input">
          <TextInput
            name="password"
            type="password"
            icon="lock"
            label="Password"
            placeholder="Type your password"
            value={values.password}
            onChange={handleChange}
            onBlur={handleBlur}
            error={touched.password && errors.password}
          />
        </div>
        <div className="LoginForm__options-right">
          {/* <a href="/">Forgot your password?</a> */}
        </div>
        <div className="LoginForm__submit">
          <Button
            size="large"
            type="primary"
            htmlType="submit"
            block
            loading={isSubmitting}
            // disabled={!isValid}
          >
            Login
          </Button>
        </div>
      </form>
    </Fragment>
  )
}

export {
  loginValidation
}

export default LoginForm;