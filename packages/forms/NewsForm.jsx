import React from 'react'
import { Button, Alert } from 'antd'
import * as Yup from 'yup'

import TextInput from './components/TextInput'
import FileInput from './components/FileInput'
import RichText from './components/RichText'

const newsValidation = () => {
  return Yup.object().shape({
    title: Yup.string().required(),
    content: Yup.string().required(),
    // thumbnail: Yup.string().required(),
  })
};

const NewsForm = (props) => {

  const {
    values,
    handleSubmit,
    handleChange,
    errors,
    touched,
    handleBlur,
    // isValid,
    setFieldValue,
    isSubmitting
  } = props;

  var toolbarOptions = [
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
    [{ 'font': [] }],
    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
    ['blockquote', 'code-block'],
    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
    [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
    [{ 'align': [] }],
    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    ['image'],
    ['clean']                                         // remove formatting button
  ];

  return (
    <form onSubmit={handleSubmit} className="newsForm">
      <div className="newsForm__errors">
        {
          !!errors.messages && <Alert
            message="Error"
            description={errors.messages}
            type="error"
            closable
          />
        }
      </div>
      <div className="newsForm__title">
        <TextInput
          name="title"
          type="text"
          label="Title"
          value={values.title}
          onChange={handleChange}
          onBlur={handleBlur}
          error={touched.title && errors.title}
        />
      </div>

      <div className="newsForm__content">
        <RichText
          label="Content"
          name="content"
          modules={{
            toolbar: toolbarOptions
          }}
          placeholder="Type the news"
          info="Lorem Ipsum é simplesmente uma simulação"
          value={values.content}
          onChange={value => setFieldValue('content', value) }
          // onBlur={() => setFieldTouched('content', true)}
          error={touched.content && errors.content}
        />
      </div>

      <div className="newsForm__thumb">
        <FileInput
          name="thumbnail"
          label="Thumbnail"
          width={300}
          height={300}
          value={values.thumbnail}
          setFieldValue={setFieldValue}
          error={touched.thumbnail && errors.thumbnail}
        />
      </div>

      <div className="newsForm__submit">
        <Button
          size="large"
          type="primary"
          htmlType="submit"
          block
          loading={isSubmitting}
        >
          Save
        </Button>
      </div>

    </form>
  )

}

export {
  newsValidation
}

export default NewsForm