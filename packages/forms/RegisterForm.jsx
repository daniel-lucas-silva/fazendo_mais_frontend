import React from 'react'
import { Button, Checkbox, Alert } from 'antd'
import * as Yup from 'yup'

import TextInput from './components/TextInput'

const registerValidation = () => {
  return Yup.object().shape({
    username: Yup.string().required('Please enter your username.').max(20),
    email: Yup.string().email().required('Please enter your email address.'),
    password: Yup.string().required('Please enter your password.'),
    confirmPassword: Yup.string().oneOf([Yup.ref('password', null)], 'Password does not match the confirm password.').required('Please confirm your password.'),
    agreed: Yup.boolean().oneOf([true], 'Must Accept Terms and Conditions'),
  })
};

const RegisterForm = (props) => {

  const {
    values,
    handleSubmit,
    handleChange,
    handleBlur,
    setFieldValue,
    errors,
    touched,
    isSubmitting
  } = props;

  return (
    <form onSubmit={handleSubmit} className="registerForm">
      <div className="registerForm__errors">
        {
          !!errors.async && <Alert
            message="Error"
            description={errors.async}
            type="error"
            closable
          />
        }
      </div>

      <div className="registerForm__input">
        <TextInput
          name="username"
          type="text"
          label="* Username"
          value={values.username}
          onChange={handleChange}
          onBlur={handleBlur}
          error={touched.username && errors.username}
        />
      </div>

      <div className="registerForm__input">
        <TextInput
          name="email"
          type="text"
          label="* Email"
          value={values.email}
          onChange={handleChange}
          onBlur={handleBlur}
          error={touched.email && errors.email}
        />
      </div>

      <div className="registerForm__input">
        <TextInput
          name="password"
          type="password"
          label="* Password"
          value={values.password}
          onChange={handleChange}
          onBlur={handleBlur}
          error={touched.password && errors.password}
        />
      </div>


      <div className="registerForm__input">
        <TextInput
          name="confirmPassword"
          type="password"
          label="* Confirm Password"
          value={values.confirmPassword}
          onChange={handleChange}
          onBlur={handleBlur}
          error={touched.confirmPassword && errors.confirmPassword}
        />
      </div>

      <div className="registerForm__options">
        {touched.agreed && errors.agreed && <span className="registerForm__options-error">{errors.agreed}</span>}
        <Checkbox
          className="form__checkbox"
          checked={values.agreed}
          onChange={_ => setFieldValue('agreed', !values.agreed)}
        >
          I have read the <a href="/">agreement</a>
        </Checkbox>
      </div>

      <div className="registerForm__submit">
        <Button
          type="primary"
          size="large"
          htmlType="submit"
          loading={isSubmitting}
          block
        >
          Register
        </Button>
      </div>

    </form>
  )
}

export {
  registerValidation
}

export default RegisterForm