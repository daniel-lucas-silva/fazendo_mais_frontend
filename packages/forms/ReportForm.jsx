import React from 'react'
import { Button, Alert } from 'antd'
import * as Yup from 'yup'

import TextInput from './components/TextInput'
import RichText from './components/RichText'

const reportValidation = () => {
  return Yup.object().shape({
    title: Yup.string().required(),
    content: Yup.string().required()
  })
};

const ReportForm = (props) => {

  const {
    values,
    handleSubmit,
    handleChange,
    errors,
    touched,
    handleBlur,
    // isValid,
    setFieldValue,
    isSubmitting
  } = props;

  var toolbarOptions = [
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
    [{ 'font': [] }],
    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
    ['blockquote', 'code-block'],
    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
    [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
    [{ 'align': [] }],
    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    ['image'],
    ['clean']                                         // remove formatting button
  ];

  return (
    <form onSubmit={handleSubmit} className="reportForm">
      <div className="reportForm__errors">
        {
          !!errors.messages && <Alert
            message="Error"
            description={errors.messages}
            type="error"
            closable
          />
        }
      </div>
      <div className="reportForm__title">
        <TextInput
          name="title"
          type="text"
          label="Title"
          value={values.title}
          onChange={handleChange}
          onBlur={handleBlur}
          error={touched.title && errors.title}
        />
      </div>

      <div className="reportForm__content">
        <RichText
          label="Content"
          name="content"
          modules={{
            toolbar: toolbarOptions
          }}
          placeholder="Type the balance"
          info="Lorem Ipsum é simplesmente uma simulação"
          value={values.content}
          onChange={value => setFieldValue('content', value) }
          // onBlur={() => setFieldTouched('content', true)}
          error={touched.content && errors.content}
        />
      </div>

      <div className="reportForm__submit">
        <Button
          size="large"
          type="primary"
          htmlType="submit"
          block
          loading={isSubmitting}
        >
          Save
        </Button>
      </div>

    </form>
  )

}

export {
  reportValidation
}

export default ReportForm