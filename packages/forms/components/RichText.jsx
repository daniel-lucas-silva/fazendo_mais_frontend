import React, { Fragment } from 'react';
import classnames from 'classnames';
import ReactQuill from 'react-quill';

import 'react-quill/dist/quill.snow.css';

const RichText = ({ label, placeholder, info, error, ...input }) => (
  <Fragment>
    {label && <label className="field__label">{label}</label>}
    <ReactQuill
      className={classnames('field__input', { 'field__input-error': error })}
      placeholder={placeholder}
      {...input}
    />
    {(error && <span className="field__error">{error}</span>) || (info && <span className="field__info">{info}</span>)}
  </Fragment>
)

export default RichText