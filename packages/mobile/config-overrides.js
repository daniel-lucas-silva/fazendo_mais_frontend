const rewireYarnWorkspaces = require('react-app-rewire-yarn-workspaces');
const { injectBabelPlugin } = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');

module.exports = function override(config, env) {

  config = rewireYarnWorkspaces(config, env);

  config = injectBabelPlugin(
    [
      "@babel/plugin-proposal-decorators",
      {
        legacy: true
      }
    ],
    config
  );

  config = injectBabelPlugin(
    [
      'import',
      {
        libraryName: "lodash",
        libraryDirectory: "",
        camel2DashComponentName: false
      },
      "lodash"
    ],
    config,
  );

  // config = rewireLess.withLoaderOptions({
  //   modifyVars: { "@themeColorMd": "#1DA57A" },
  //   javascriptEnabled: true,
  // })(config, env);

  return config;
};
