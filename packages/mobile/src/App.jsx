import React from 'react';
import {
  App,
  Panel,
  View,
  Statusbar,
  Page,
  LoginScreen,
  LoginScreenTitle,
  List,
  ListItem,
  Label,
  Input,
  ListButton,
  BlockFooter
} from 'framework7-react';

import routes from './config/routes';
import Sidebar from './components/Sidebar';

export default function (props) {

  const f7params = {
    id: 'br.com.fazendomais',
    name: 'Framework7',
    theme: 'auto',
    routes,
    touch: {
      materialRipple: false
    },
    materialPreloaderHtml : true,
    panel: {
      swipe: 'both',
      swipeActiveArea: 20
    }
  };

  return (
    <App params={f7params}>

      <Statusbar />

      <Panel left cover>
        <Sidebar />
      </Panel>

      <View id="main-view" url="/screens" main className="ios-edges" pushState/>

      <LoginScreen className="Login">
        <View>
          <Page loginScreen>
            <LoginScreenTitle>Login</LoginScreenTitle>
            <List form>
              <ListItem>
                <Label>Username</Label>
                <Input name="username" placeholder="Username" type="text"></Input>
              </ListItem>
              <ListItem>
                <Label>Password</Label>
                <Input name="password" type="password" placeholder="Password"></Input>
              </ListItem>
            </List>
            <List>
              <ListButton title="Sign In" loginScreenClose></ListButton>
              <BlockFooter>
                <p>Click Sign In to close Login Screen</p>
              </BlockFooter>
            </List>
          </Page>
        </View>
      </LoginScreen>
    </App>
  );
};
