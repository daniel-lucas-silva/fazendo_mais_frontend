import React, {Component} from 'react';
import {
  Page,
  BlockTitle,
  List,
  Button,
  Messagebar,
  Link
} from 'framework7-react';

class Screens extends Component {
  render() {
    return (
      <Page className="_screens">
        <BlockTitle>Screens</BlockTitle>
        <List inset>
          <Button fill href="/">Home</Button>
          <Button fill href="/entity/">Entity</Button>
          <Button fill loginScreenOpen=".Login">Login</Button>
          <Button outline href="/profile/">Profile</Button>
          <Button outline href="/settings/">Settings</Button>
          <Button outline href="/sidebar/">Sidebar</Button>
        </List>
      </Page>
    );
  }
}

export default Screens;
