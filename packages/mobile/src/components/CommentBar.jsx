import React, {Component} from 'react';
import PropTypes from 'prop-types';

class CommentBar extends Component {
  render() {
    return (
      <Messagebar
        placeholder={"teste"}
        ref={(el) => {this.messagebarComponent = el}}
        maxHeight={142}
      >
        <Link
          iconMd="material:person"
          slot="inner-start"
          // onClick={() => {this.setState({sheetVisible: !this.state.sheetVisible})}}
        />
        <Link
          iconMd="material:send"
          slot="inner-end"
          // onClick={this.sendMessage.bind(this)}
        />
      </Messagebar>
    );
  }
}

CommentBar.propTypes = {};

export default CommentBar;
