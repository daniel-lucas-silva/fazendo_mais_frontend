import React from 'react';
import { PageContent, List, ListItem } from 'framework7-react';

const Filter = ({ view }) => {
  return (
    <PageContent>
      <List>
        <ListItem
          title="Categorias"
          smartSelect
          smartSelectParams={{openIn: 'popup', searchbar: true, searchbarPlaceholder: 'Search car', view}}
        >
          <select name="car" multiple defaultValue={['honda', 'audi', 'ford']}>
            <optgroup label="Japanese">
              <option value="honda">Honda</option>
              <option value="lexus">Lexus</option>
              <option value="mazda">Mazda</option>
              <option value="nissan">Nissan</option>
              <option value="toyota">Toyota</option>
            </optgroup>
          </select>
        </ListItem>
        <ListItem
          title="Cidades"
          smartSelect
          smartSelectParams={{openIn: 'popup', searchbar: true, searchbarPlaceholder: 'Search car', view}}
        >
          <select name="car" multiple defaultValue={['honda', 'audi', 'ford']}>
            <optgroup label="Japanese">
              <option value="honda">Honda</option>
            </optgroup>
          </select>
        </ListItem>
      </List>
    </PageContent>
  );
};

export default Filter;
