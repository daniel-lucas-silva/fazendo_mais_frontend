import React, { PureComponent } from 'react';
import { Navbar, Link, NavLeft, NavTitle, NavRight, Searchbar} from 'framework7-react';

class HomeNavbar extends PureComponent {
  render() {
    return (
      <Navbar className="HomeNavbar">
        <NavLeft>
          <Link iconMaterial="menu" panelOpen="left" />
        </NavLeft>
        <NavTitle>

        </NavTitle>
        <NavRight>
          <Link searchbarEnable=".Home__Search" iconIos="f7:search_strong" iconMd="material:search" />
        </NavRight>
        <Searchbar
          className="Home__Search"
          expandable
          searchContainer=".search-list"
          searchIn=".item-title"
        />
      </Navbar>
    );
  }
}

export default HomeNavbar;