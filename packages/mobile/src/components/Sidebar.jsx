import React from 'react';
import { Page, Navbar, Block, BlockTitle, List, ListItem, Link } from 'framework7-react';

export default () => (
  <Page>
    <BlockTitle>Load page in panel</BlockTitle>
    <List>
      <ListItem>
        <Link loginScreenOpen=".Login" >Login</Link>
      </ListItem>
    </List>
  </Page>
);
