import store from '@dannlks/store';
// import { fetchEntities } from '@dannlks/store/entities/entitiesActions';


import NotFoundPage from '../components/pages/NotFoundPage';

export default [
  {
    path: '/screens',
    async(routeTo, routeFrom, resolve, reject) {
      
      const Screens = () => import('../__screens');
      Screens().then((rc) => {
        resolve({ component: rc.default })
      });
    }
  },
  {
    path: '/',
    async(routeTo, routeFrom, resolve, reject) {
      const Home = () => import('../features/__root/Home');
      Home().then((rc) => {
        resolve({ component: rc.default })
      });
    }
  },
  {
    path: '/login',
    async(routeTo, routeFrom, resolve, reject) {
      const Login = () => import('../features/auth/Login');
      Login().then((rc) => {
        resolve({ component: rc.default })
      });
    }
  },
  {
    path: '/sidebar',
    async(routeTo, routeFrom, resolve, reject) {
      const Sidebar = () => import('../components/Sidebar');
      Sidebar().then((rc) => {
        resolve({ component: rc.default })
      });
    }
  },
  {
    path: '/entity',
    async(routeTo, routeFrom, resolve, reject) {
      const Entity = () => import('../features/entity/Entity');
      Entity().then((rc) => {
        resolve({ component: rc.default })
      });
    },
    tabs: [
      {
        path: '/',
        id: 'detail',
        async(routeTo, routeFrom, resolve, reject) {
          const Detail = () => import('../features/entity/tabs/DetailTab');
          Detail().then((rc) => {
            resolve({ component: rc.default })
          });
        }
      },
      {
        path: '/news/',
        id: 'news',
        async(routeTo, routeFrom, resolve, reject) {
          const News = () => import('../features/entity/tabs/NewsTab');
          News().then((rc) => {
            resolve({ component: rc.default })
          });
        }
      },
      {
        path: '/gallery/',
        id: 'gallery',
        async(routeTo, routeFrom, resolve, reject) {
          const Gallery = () => import('../features/entity/tabs/GalleryTab');
          Gallery().then((rc) => {
            resolve({ component: rc.default })
          });
        }
      },
      {
        path: '/balance/',
        id: 'balance',
        async(routeTo, routeFrom, resolve, reject) {
          const Balance = () => import('../features/entity/tabs/BalanceTab');
          Balance().then((rc) => {
            resolve({ component: rc.default })
          });
        }
      },
      {
        path: '/reports/',
        id: 'reports',
        async(routeTo, routeFrom, resolve, reject) {
          const Reports = () => import('../features/entity/tabs/ReportsTab');
          Reports().then((rc) => {
            resolve({ component: rc.default })
          });
        }
      }
    ]
  },
  // {
  //   path: '/',
  //   component: HomePage,
  // },
  // {
  //   path: '/panel-left/',
  //   component: PanelLeftPage,
  // },
  // {
  //   path: '/panel-right/',
  //   component: PanelRightPage,
  // },
  // {
  //   path: '/about/',
  //   component: AboutPage,
  // },
  // {
  //   path: '/form/',
  //   component: FormPage,
  // },
  // {
  //   path: '/dynamic-route/blog/:blogId/post/:postId/',
  //   component: DynamicRoutePage,
  // },
  {
    path: '(.*)',
    component: NotFoundPage,
  },

];
