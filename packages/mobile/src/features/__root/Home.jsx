import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { fetchEntities } from '@dannlks/store/entities/entitiesActions';
import { Page, List, ListItem, Link, Navbar } from 'framework7-react';

import { Sheet, Toolbar } from 'framework7-react';

import Filter from '../../components/Filter'
import HomeNav from './HomeNav'
import './Home.scss'

const mapStateToProps = (state) => ({
  entities: state.entities
})

@connect(mapStateToProps, { fetchEntities })
class Home extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      sheetOpened: false,
      vlData: {
        items: [],
      },
    };
  }

  componentDidMount() {
    this._loadEntities()
    console.log(this.refs.virtualList.f7VirtualList)
  }

  _renderExternal = (vl, vlData) => {
    this.setState({ vlData });
  }

  _loadEntities = (e, done = () => {}) => {
    this.page = 1;
    this.limit = 20;
    this.allowInfinite = true;
    this.props.fetchEntities(`?limit=${this.limit}`)
    .then((result) => {
      this.refs.virtualList.f7VirtualList.replaceAllItems(result.rows)
      done();
    })
  }

  _loadMore = () => {
    if (!this.allowInfinite) return;
    this.allowInfinite = false;

    this.page = this.page + 1;
    this.props.fetchEntities(`?limit=${this.limit}&page=${this.page}`)
      .then((result) => {
        this.refs.virtualList.f7VirtualList.appendItems(result.rows)
        if (result.rows.length === this.limit) {
          this.allowInfinite = true;
        }
      })
  }

  render() {
    return (
      <Page
        className="Home"
        hideNavbarOnScroll
        ptr
        onPtrRefresh={this._loadEntities}
        infinite
        infiniteDistance={1300}
        infinitePreloader={false}
        onInfinite={() => this._loadMore()}
      >

        <Navbar>
          <HomeNav {...this.props} />
        </Navbar>

        <List className="searchbar-not-found">
          <ListItem title="Nothing found" />
        </List>

        <List
          mediaList
          virtualList
          ref="virtualList"
          virtualListParams={
            {
              items: [],
              cache: false,
              // searchAll: this.searchAll,
              rowsAfter: 0,
              renderExternal: this._renderExternal,
              height: this.$theme.ios ? 85 : 95,
              rowsToRender: 10
            }
          }
        >
          <ul>
            {this.state.vlData.items.map((item, index) => {
              const { title } = JSON.parse(item.category);
              const { city, state } = JSON.parse(item.location);
              return (
                <ListItem
                  key={index}
                  link="/entity/"
                  title={item.name}
                  subtitle={`${city}-${state}`}
                  text={`${title}`}
                  style={{ top: `${this.state.vlData.topPosition}px` }}
                >
                  <img slot="media" src="https://picsum.photos/100/100?image=0" width="67" />
                </ListItem>
              )
            })}
          </ul>
        </List>


        <Sheet backdrop className="demo-sheet" opened={this.state.sheetOpened} onSheetClosed={() => {this.setState({sheetOpened: false})}}>
          <Toolbar>
            <h3>Filters</h3>
            <Link sheetClose>Close</Link>
          </Toolbar>
          <Filter view={this.$f7.view.current} />
        </Sheet>
      </Page>
    );
  }
}

export default Home;