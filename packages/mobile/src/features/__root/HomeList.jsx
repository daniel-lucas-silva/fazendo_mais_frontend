import React, {PureComponent, Fragment} from 'react';
import PropTypes from 'prop-types';
import { List, ListItem } from 'framework7-react';
import { connect } from 'react-redux';

const mapStateToProps = (state) => ({
  entities: state.entities
})

@connect(mapStateToProps)
class HomeList extends PureComponent {

  constructor(props) {
    super(props);

    console.log(this)
    this.state = {
      vlData: {
        items: [],
      },
    };
  }
  searchAll(query, items) {
    const found = [];
    for (let i = 0; i < items.length; i += 1) {
      if (items[i].title.toLowerCase().indexOf(query.toLowerCase()) >= 0 || query.trim() === '') found.push(i);
    }
    return found; // return array with mathced indexes
  }
  renderExternal = (vl, vlData) => {
    this.setState({ vlData });
    console.log('vl', vl);
    console.log('vlData', vlData)
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
    this.refs.virtualList.f7VirtualList.replaceAllItems(nextProps.entities.rows)
  }

  componentDidMount() {
    // console.log(this.refs.virtualList.f7VirtualList.deleteAllItems())
  }

  render() {
    return (
      <Fragment>
        <List className="searchbar-not-found">
          <ListItem title="Nothing found"/>
        </List>

        <List
          mediaList
          virtualList
          ref="virtualList"
          virtualListParams={
            {
              items: this.props.entities.rows,
              // searchAll: this.searchAll,
              rowsAfter: 0,
              renderExternal: this.renderExternal,
              height: this.$theme.ios ? 85 : 95
            }
          }
        >
          <ul>
            {this.state.vlData.items.map((item, index) => {
              const { city, state } = JSON.parse(item.location); 
              return (
                <ListItem
                  key={index}
                  link="/entity/"
                  title={item.name}
                  subtitle={`${city}-${state}`}
                  text="Categoria"
                  style={{ top: `${this.state.vlData.topPosition}px` }}
                >
                  <img slot="media" src="https://picsum.photos/100/100?image=0" width="67" />
                </ListItem>
              )
            })}
          </ul>
        </List>
      </Fragment>
    );
  }
}

HomeList.propTypes = {};

export default HomeList;
