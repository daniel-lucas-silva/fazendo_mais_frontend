import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import { Navbar, NavLeft, NavTitle, NavRight, Link, Searchbar } from 'framework7-react';

class HomeNav extends Component {
  render() {
    return (
      <Fragment>
        <NavLeft>
          <Link iconMaterial="menu" panelOpen="left" />
        </NavLeft>
        <NavTitle>
          Fazendo Mais
        </NavTitle>
        <NavRight>
          <Link sheetOpen=".demo-sheet" iconIos="material:filter_list" iconMd="material:filter_list" />
          <Link searchbarEnable=".Home__Search" iconIos="f7:search_strong" iconMd="material:search" />
        </NavRight>
        <Searchbar
          className="Home__Search"
          expandable
          // backdrop={false}
          // searchContainer=".virtual-list"
          // searchItem="li"
          // searchIn=".item-title"
          customSearch
          noShadow
          backdrop={false}
        />
      </Fragment>
    );
  }
}

HomeNav.propTypes = {};

export default HomeNav;
