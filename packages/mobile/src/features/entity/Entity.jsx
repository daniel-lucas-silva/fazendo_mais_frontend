import React, { Component } from 'react';
import { Navbar, NavRight, Searchbar, Page, Block, Tabs, Tab, Link, Toolbar } from 'framework7-react';

import "./Entity.scss";

class Entity extends Component {

  render() {
    return (
      <Page hideToolbarOnScroll className="Entity">
        <Navbar title="Entity" backLink="Back"/>
        <Toolbar tabbar labels bottomMd>
          <Link tabLink  href="/entity/" routeTabId="detail" text="Detalhes" iconMd="material:home" />
          <Link tabLink href="/entity/news/" routeTabId="news" text="Notícias" iconMd="material:library_books" />
          <Link tabLink href="/entity/gallery/" routeTabId="gallery" text="Galeria" iconMd="material:collections" />
          <Link tabLink href="/entity/reports/" routeTabId="reports" text="Relatórios" iconMd="material:insert_chart" />
          <Link tabLink href="/entity/balance/" routeTabId="balance" text="Balanço" iconMd="material:monetization_on" />
        </Toolbar>
        <Tabs routable>
          <Tab id="detail"  />
          <Tab id="news"  />
          <Tab id="gallery" />
          <Tab id="balance" />
          <Tab id="reports" />
        </Tabs>
      </Page>
    )
  }
}

export default Entity;