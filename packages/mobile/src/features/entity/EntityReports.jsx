import React, {Component} from 'react';
import PropTypes from 'prop-types';

class EntityReports extends Component {
  render() {
    return (
      <div className="block">
        <BlockTitle>Styled Cards</BlockTitle>
        <Card className="demo-card-header-pic">
          <CardHeader
            className="no-border"
            valign="bottom"
            style={{ backgroundImage: 'url(http://lorempixel.com/1000/600/nature/3/)' }}
          >Journey To Mountains</CardHeader>
          <CardContent>
            <p className="date">Posted on January 21, 2015</p>
            <p>Quisque eget vestibulum nulla. Quisque quis dui quis ex ultricies efficitur vitae non felis. Phasellus quis nibh hendrerit...</p>
          </CardContent>
          <CardFooter>
            <Link>Like</Link>
            <Link>Read more</Link>
          </CardFooter>
        </Card>
        <Card className="demo-card-header-pic">
          <CardHeader
            className="no-border"
            valign="bottom"
            style={{ backgroundImage: 'url(http://lorempixel.com/1000/600/people/6/)' }}
          >Journey To Mountains</CardHeader>
          <CardContent>
            <p className="date">Posted on January 21, 2015</p>
            <p>Quisque eget vestibulum nulla. Quisque quis dui quis ex ultricies efficitur vitae non felis. Phasellus quis nibh hendrerit...</p>
          </CardContent>
          <CardFooter>
            <Link>Like</Link>
            <Link>Read more</Link>
          </CardFooter>
        </Card>
      </div>
    );
  }
}

EntityReports.propTypes = {};

export default EntityReports;
