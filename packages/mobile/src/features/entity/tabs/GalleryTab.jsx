import React, {Component} from 'react';
import { List, ListItem } from 'framework7-react';

class GalleryTab extends Component {

  constructor(props) {
    super(props);

    const items = [];
    for (let i = 1; i <= 50; i += 1) {
      items.push({
        title: `Item ${i}`,
        subtitle: `Subtitle ${i}`,
      });
    }
    this.state = {
      items,
      vlData: {
        items: [],
      },
    };
  }
  searchAll(query, items) {
    const found = [];
    for (let i = 0; i < items.length; i += 1) {
      if (items[i].title.toLowerCase().indexOf(query.toLowerCase()) >= 0 || query.trim() === '') found.push(i);
    }
    return found; // return array with mathced indexes
  }
  renderExternal(vl, vlData) {
    this.setState({ vlData });
  }

  render() {
    return (
      <div>
        <List className="searchbar-not-found">
          <ListItem title="Nothing found"/>
        </List>

        <List
          className="searchbar-found"
          mediaList
          virtualList
          virtualListParams={
            {
              items: this.state.items,
              searchAll: this.searchAll,
              renderExternal: this.renderExternal.bind(this),
              height: this.$theme.ios ? 103 : 122,
              cols: 3,
              createUl: false
            }
          }
        >
          <ul className="list-report">
            {this.state.vlData.items.map((item, index) => (
              <li style={{top: `${this.state.vlData.topPosition}px`}} >
                {item.title}
              </li>
            ))}
          </ul>
        </List>
      </div>
    );
  }
}

GalleryTab.propTypes = {};

export default GalleryTab;
