import React, {Component} from 'react';
import { List, ListItem, Searchbar } from 'framework7-react';

class NewsTab extends Component {

  constructor(props) {
    super(props);

    const items = [];
    for (let i = 1; i <= 50; i += 1) {
      items.push({
        title: `Item ${i}`,
        subtitle: `Subtitle ${i}`,
      });
    }
    this.state = {
      items,
      vlData: {
        items: [],
      },
    };
  }
  searchAll(query, items) {
    const found = [];
    for (let i = 0; i < items.length; i += 1) {
      if (items[i].title.toLowerCase().indexOf(query.toLowerCase()) >= 0 || query.trim() === '') found.push(i);
    }
    return found; // return array with mathced indexes
  }
  renderExternal(vl, vlData) {
    this.setState({ vlData });
  }

  render() {
    return (
      <div>
        <Searchbar
          className="Home__Search"
          customSearch
          onChange={(e) => console.log(e.target.value)}
          backdrop={false}
          disableButton={false}
          // searchContainer=".virtual-list"
          // searchItem="li"
          // searchIn=".item-title"
        />
        <List className="searchbar-not-found">
          <ListItem title="Nothing found"/>
        </List>

        <List
          className="searchbar-found"
          mediaList
          virtualList
          virtualListParams={
            {
              items: this.state.items,
              searchAll: this.searchAll,
              renderExternal: this.renderExternal.bind(this),
              height: this.$theme.ios ? 103 : 113
            }
          }
        >
          <ul>
            {this.state.vlData.items.map((item, index) => (
              <ListItem
                key={index}
                link="#"
                title={item.title}
                subtitle={item.subtitle}
                style={{top: `${this.state.vlData.topPosition}px`}}
                after="$16"
                text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus."
              >
                <img slot="media" src="https://picsum.photos/100/100?image=0" width="80" />
              </ListItem>
            ))}
          </ul>
        </List>
      </div>
    );
  }
}

NewsTab.propTypes = {};

export default NewsTab;
