import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from '@dannlks/store';
import Framework7 from 'framework7/framework7.esm.bundle';
import Framework7React from 'framework7-react';
import 'framework7/css/framework7.min.css';

import App from './App';
import './index.scss';
import * as serviceWorker from './serviceWorker';

Framework7.use(Framework7React);

const Start = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

ReactDOM.render(<Start />, document.getElementById('root'));

serviceWorker.register();
