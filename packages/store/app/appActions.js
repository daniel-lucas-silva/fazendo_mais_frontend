import axios from 'axios';
export const SET_APP_LOADED = "SET_APP_LOADED";

axios.defaults.baseURL = 'http://fazendo.mais/';
axios.interceptors.response.use((response) => {
  return response && response.data;
}, (err) => {
  console.log(err.response.status);
  return Promise.reject(err); // i didn't have this line before
});

export const saveToken = (token) => {
  return () => {
    localStorage.setItem('token', token);
    setToken(token);
  }
};

export const setToken = (token) => {
  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
};

export const removeToken = () => {
  return () => {
    localStorage.removeItem('token');
  }
};

export const setAppLoaded = () => ({
  type: SET_APP_LOADED
});