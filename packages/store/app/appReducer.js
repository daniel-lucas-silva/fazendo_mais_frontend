import {SET_APP_LOADED} from "./appActions";

const initialState = {
  loaded: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_APP_LOADED:
      return {
        ...state,
        loaded: true
      };
    default:
      return state;
  }
}