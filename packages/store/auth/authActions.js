import axios from 'axios';

import {removeToken, saveToken, setAppLoaded} from "../app/appActions";

export const SET_CURRENT_USER = "SET_CURRENT_USER";
export const REMOVE_CURRENT_USER = "REMOVE_CURRENT_USER";

export const register = (user) => {
  return dispatch => {
    if(user.role === 'entity')
      user.is_entity = true;
    delete user.role;
    delete user.confirmPassword;

    return new Promise((resolve, reject) => {
      axios.post('auth/register', user)
        .then((response) => {
          console.log(response);
          dispatch(saveToken(response.data.token));
          resolve(response.data);
        })
        .catch((error) => {
          reject(error && error.response && error.response.data);
        });
    });
  }
};

export const login = (user) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.post('auth/login', user)
        .then(function(response) {
          console.log('Success', response.data);
          dispatch(saveToken(response.data.token));
          dispatch(setCurrentUser(response.data.user));
          resolve(response.data);
        })
        .catch(function(error) {
          console.dir(error.response)
          reject(error && error.response && error.response.data);
        });
    });
  }
};

export const logout = () => {
  return dispatch => {
    dispatch(removeToken());
    dispatch(removeCurrentUser());
  }
};

export const me = () => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      if (localStorage.token) {
        axios.get('auth/me', {
          headers: {'Authorization': "Bearer " + localStorage.getItem('token')}
        })
          .then((response) => {
            dispatch(saveToken(response.data.token));
            dispatch(setCurrentUser(response.data.user));
            resolve(response);
          })
          .catch((error) => {
            dispatch(logout());
            reject(error && error.response && error.response.data);
          })
          .finally(() => {
            dispatch(setAppLoaded());
          })
      } else {
        dispatch(setAppLoaded());
        resolve(true);
      }
    });
  }
};

export const setCurrentUser = (user) => {
  return dispatch => {
    dispatch({type: SET_CURRENT_USER, user});
  }
};

export const removeCurrentUser = () => {
  return dispatch => {
    dispatch({type: REMOVE_CURRENT_USER});
  }
};