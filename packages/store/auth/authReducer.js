import {SET_CURRENT_USER, REMOVE_CURRENT_USER} from "./authActions";

const initialState = {};

export default ( state = initialState, action ) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return { ...state, user: action.user };
    case REMOVE_CURRENT_USER:
      return {};
    default:
      return state;
  }
};