import axios from 'axios';

export const SET_BALANCE = "SET_BALANCE";
export const ADD_BALANCE = "ADD_BALANCE";
export const BALANCE_FETCHED = "BALANCE_FETCHED";
export const BALANCE_UPDATED = "BALANCE_UPDATED";
export const BALANCE_DELETED = "BALANCE_DELETED";
export const CLEAR_BALANCE = "CLEAR_BALANCE";

export const fetchBalance = (params = "") => {
  return (dispatch, getState) => {
    return new Promise((resolve, reject) => {
      const entity_id = getState().auth.user.entity.id;
      axios.get(`balance/${entity_id}${params}`)
        .then((response) => {
          dispatch({
            type: SET_BALANCE,
            balance: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          dispatch({
            type: CLEAR_BALANCE
          });
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const getBalance = balance_id => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`balance/get/${balance_id}`)
        .then((response) => {
          dispatch({
            type: BALANCE_FETCHED,
            balance: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const saveBalance = data => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.post(`balance`, data)
        .then((response) => {
          console.log('data ', data)
          console.log('response ', response)
          dispatch({
            type: ADD_BALANCE,
            balance: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const updateBalance = (balance_id, data) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.patch(`balance/${balance_id}`, data)
        .then((response) => {
          dispatch({
            type: BALANCE_UPDATED,
            balance: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const deleteBalance = balance_id => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.delete(`balance/${balance_id}`)
        .then((response) => {
          // dispatch({
          //   type: BALANCE_DELETED,
          //   balance_id
          // });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};