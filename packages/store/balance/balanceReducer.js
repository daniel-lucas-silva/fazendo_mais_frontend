import {
  ADD_BALANCE,
  BALANCE_DELETED,
  BALANCE_FETCHED,
  BALANCE_UPDATED,
  SET_BALANCE,
  CLEAR_BALANCE
} from "./balanceActions";

const initialState = {
  rows: []
};

export default ( state = initialState, action ) => {
  switch (action.type) {
    case BALANCE_DELETED:
      return {
        ...state,
        rows: state.rows.filter( item => item.id !== action.balance_id )
      };
    case BALANCE_UPDATED:
      return state.rows.map( item => {
        if( item.id === action.balance.id ) return action.balance;
        return item;
      });
    case ADD_BALANCE:
      return {
        ...state,
        rows: [
          action.balance,
          ...state.rows,
        ]
      };
    case BALANCE_FETCHED:
      // busca no state se existe o mesmo balanço, que foi pego no servidor
      const index = state.rows.findIndex(item => item.id === action.balance.id);
      // se existe
      if (index > -1) {
        // mapeia os itens com o mesmo id
        return {
          ...state,
          rows: state.rows.map(item => {
            // e atualiza o item, com o que foi pego no server
            if (item.id === action.balance.id) return action.balance;
            return item;
          })
        };
      } else {
        return {
          ...state,
          rows: [
            action.balance,
            ...state.rows,
          ]
        };
      }
    case SET_BALANCE:
      return action.balance;
    case CLEAR_BALANCE:
      return {};
    default:
      return state;
  }
};