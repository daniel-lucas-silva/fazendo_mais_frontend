import axios from 'axios';

export const SET_CATEGORIES = "SET_CATEGORIES";
export const ADD_CATEGORY = "ADD_CATEGORY";
export const CATEGORY_FETCHED = "CATEGORY_FETCHED";
export const CATEGORY_UPDATED = "CATEGORY_UPDATED";
export const CATEGORY_DELETED = "CATEGORY_DELETED";
export const CLEAR_CATEGORIES = "CLEAR_CATEGORIES";

export const fetchCategories = (params = "") => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`categories${params}`)
        .then((response) => {
          dispatch({
            type: SET_CATEGORIES,
            categories: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          dispatch({
            type: CLEAR_CATEGORIES
          });
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const getCategory = category_id => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`categories/${category_id}`)
        .then((response) => {
          dispatch({
            type: CATEGORY_FETCHED,
            category: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const saveCategory = data => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.post(`categories`, data)
        .then((response) => {
          console.log('data ', data)
          console.log('response ', response)
          dispatch({
            type: ADD_CATEGORY,
            category: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const updateCategory = (category_id, data) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.patch(`categories/${category_id}`, data)
        .then((response) => {
          dispatch({
            type: CATEGORY_UPDATED,
            category: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const deleteCategory = category_id => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.delete(`categories/${category_id}`)
        .then((response) => {
          // dispatch({
          //   type: CATEGORY_DELETED,
          //   category_id
          // });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};