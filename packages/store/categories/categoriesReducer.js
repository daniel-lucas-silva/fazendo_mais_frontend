import {
  ADD_CATEGORY,
  CATEGORY_DELETED,
  CATEGORY_FETCHED,
  CATEGORY_UPDATED,
  SET_CATEGORIES,
  CLEAR_CATEGORIES
} from "./categoriesActions";

const initialState = {
  rows: []
};

export default ( state = initialState, action ) => {
  switch (action.type) {
    case CATEGORY_DELETED:
      return {
        ...state,
        rows: state.rows.filter( item => item.id !== action.category_id )
      };
    case CATEGORY_UPDATED:
      return state.rows.map( item => {
        if( item.id === action.category.id ) return action.category;
        return item;
      });
    case ADD_CATEGORY:
      return {
        ...state,
        rows: [
          action.category,
          ...state.rows,
        ]
      };
    case CATEGORY_FETCHED:
      // busca no state se existe o mesmo balanço, que foi pego no servidor
      const index = state.rows.findIndex(item => item.id === action.category.id);
      // se existe
      if (index > -1) {
        // mapeia os itens com o mesmo id
        return {
          ...state,
          rows: state.rows.map(item => {
            // e atualiza o item, com o que foi pego no server
            if (item.id === action.category.id) return action.category;
            return item;
          })
        };
      } else {
        return {
          ...state,
          rows: [
            action.category,
            ...state.rows,
          ]
        };
      }
    case SET_CATEGORIES:
      return action.categories;
    case CLEAR_CATEGORIES:
      return {};
    default:
      return state;
  }
};