import axios from 'axios';

export const SET_ENTITIES = "SET_ENTITIES";
export const ADD_ENTITY = "ADD_ENTITY";
export const ENTITY_FETCHED = "ENTITY_FETCHED";
export const ENTITY_UPDATED = "ENTITY_UPDATED";
export const ENTITY_DELETED = "ENTITY_DELETED";
export const CLEAR_ENTITIES = "CLEAR_ENTITIES";

export const fetchEntities = (params = "") => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`entities${params}`)
        .then((response) => {
          dispatch({
            type: SET_ENTITIES,
            entities: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          dispatch({
            type: CLEAR_ENTITIES
          });
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const getEntity = entity_id => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`entities/${entity_id}`)
        .then((response) => {
          dispatch({
            type: ENTITY_FETCHED,
            entity: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const saveEntity = data => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.post(`entities`, data)
        .then((response) => {
          console.log('data ', data)
          console.log('response ', response)
          dispatch({
            type: ADD_ENTITY,
            entity: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const updateEntity = (entity_id, data) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.patch(`entities/${entity_id}`, data)
        .then((response) => {
          dispatch({
            type: ENTITY_UPDATED,
            entity: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const deleteEntity = entity_id => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.delete(`entities/${entity_id}`)
        .then((response) => {
          // dispatch({
          //   type: ENTITY_DELETED,
          //   entity_id
          // });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};