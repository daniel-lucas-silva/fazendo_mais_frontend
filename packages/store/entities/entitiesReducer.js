import {
  ADD_ENTITY,
  ENTITY_DELETED,
  ENTITY_FETCHED,
  ENTITY_UPDATED,
  SET_ENTITIES,
  CLEAR_ENTITIES
} from "./entitiesActions";

const initialState = {
  rows: []
};

export default ( state = initialState, action ) => {
  switch (action.type) {
    case ENTITY_DELETED:
      return {
        ...state,
        rows: state.rows.filter( item => item.id !== action.entity_id )
      };
    case ENTITY_UPDATED:
      return state.rows.map( item => {
        if( item.id === action.entity.id ) return action.entity;
        return item;
      });
    case ADD_ENTITY:
      return {
        ...state,
        rows: [
          action.entity,
          ...state.rows,
        ]
      };
    case ENTITY_FETCHED:
      // busca no state se existe o mesmo balanço, que foi pego no servidor
      const index = state.rows.findIndex(item => item.id === action.entity.id);
      // se existe
      if (index > -1) {
        // mapeia os itens com o mesmo id
        return {
          ...state,
          rows: state.rows.map(item => {
            // e atualiza o item, com o que foi pego no server
            if (item.id === action.entity.id) return action.entity;
            return item;
          })
        };
      } else {
        return {
          ...state,
          rows: [
            action.entity,
            ...state.rows,
          ]
        };
      }
    case SET_ENTITIES:
      return action.entities;
    case CLEAR_ENTITIES:
      return {};
    default:
      return state;
  }
};