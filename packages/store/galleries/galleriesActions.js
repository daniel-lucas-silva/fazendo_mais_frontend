import axios from 'axios';

export const SET_GALLERIES = "SET_GALLERIES";
export const ADD_GALLERY = "ADD_GALLERY";
export const GALLERY_FETCHED = "GALLERY_FETCHED";
export const GALLERY_UPDATED = "GALLERY_UPDATED";
export const GALLERY_DELETED = "GALLERY_DELETED";
export const CLEAR_GALLERIES = "CLEAR_GALLERIES";

export const fetchGalleries = (params = "") => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`galleries${params}`)
        .then((response) => {
          dispatch({
            type: SET_GALLERIES,
            galleries: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          dispatch({
            type: CLEAR_GALLERIES
          });
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const getGallery = gallery_id => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`galleries/${gallery_id}`)
        .then((response) => {
          dispatch({
            type: GALLERY_FETCHED,
            gallery: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const saveGallery = data => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.post(`galleries`, data)
        .then((response) => {
          console.log('data ', data)
          console.log('response ', response)
          dispatch({
            type: ADD_GALLERY,
            gallery: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const updateGallery = (gallery_id, data) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.patch(`galleries/${gallery_id}`, data)
        .then((response) => {
          dispatch({
            type: GALLERY_UPDATED,
            gallery: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const deleteGallery = gallery_id => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.delete(`galleries/${gallery_id}`)
        .then((response) => {
          // dispatch({
          //   type: GALLERY_DELETED,
          //   gallery_id
          // });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};