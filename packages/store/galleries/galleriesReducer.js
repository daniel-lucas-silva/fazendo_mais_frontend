import {
  ADD_GALLERY,
  GALLERY_DELETED,
  GALLERY_FETCHED,
  GALLERY_UPDATED,
  SET_GALLERIES,
  CLEAR_GALLERIES
} from "./galleriesActions";

const initialState = {
  rows: []
};

export default ( state = initialState, action ) => {
  switch (action.type) {
    case GALLERY_DELETED:
      return {
        ...state,
        rows: state.rows.filter( item => item.id !== action.gallery_id )
      };
    case GALLERY_UPDATED:
      return state.rows.map( item => {
        if( item.id === action.gallery.id ) return action.gallery;
        return item;
      });
    case ADD_GALLERY:
      return {
        ...state,
        rows: [
          action.gallery,
          ...state.rows,
        ]
      };
    case GALLERY_FETCHED:
      // busca no state se existe o mesmo balanço, que foi pego no servidor
      const index = state.rows.findIndex(item => item.id === action.gallery.id);
      // se existe
      if (index > -1) {
        // mapeia os itens com o mesmo id
        return {
          ...state,
          rows: state.rows.map(item => {
            // e atualiza o item, com o que foi pego no server
            if (item.id === action.gallery.id) return action.gallery;
            return item;
          })
        };
      } else {
        return {
          ...state,
          rows: [
            action.gallery,
            ...state.rows,
          ]
        };
      }
    case SET_GALLERIES:
      return action.galleries;
    case CLEAR_GALLERIES:
      return {};
    default:
      return state;
  }
};