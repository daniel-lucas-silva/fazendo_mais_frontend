import axios from 'axios';

export const SET_NEWS = "SET_NEWS";
export const ADD_NEWS = "ADD_NEWS";
export const NEWS_FETCHED = "NEWS_FETCHED";
export const NEWS_UPDATED = "NEWS_UPDATED";
export const NEWS_DELETED = "NEWS_DELETED";
export const CLEAR_NEWS = "CLEAR_NEWS";

export const fetchNews = (params = "") => {
  return (dispatch, getState) => {
    return new Promise((resolve, reject) => {
      const entity_id = getState().auth.user.entity.id;
      axios.get(`news/${entity_id}${params}`)
        .then((response) => {
          dispatch({
            type: SET_NEWS,
            news: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          dispatch({
            type: CLEAR_NEWS
          });
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const getNews = news_id => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`news/get/${news_id}`)
        .then((response) => {
          dispatch({
            type: NEWS_FETCHED,
            news: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const saveNews = data => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.post(`news`, data)
        .then((response) => {
          console.log('data ', data)
          console.log('response ', response)
          dispatch({
            type: ADD_NEWS,
            news: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const updateNews = (news_id, data) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.patch(`news/${news_id}`, data)
        .then((response) => {
          dispatch({
            type: NEWS_UPDATED,
            news: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const deleteNews = news_id => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.delete(`news/${news_id}`)
        .then((response) => {
          // dispatch({
          //   type: NEWS_DELETED,
          //   news_id
          // });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};