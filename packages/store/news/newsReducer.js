import {
  ADD_NEWS,
  NEWS_DELETED,
  NEWS_FETCHED,
  NEWS_UPDATED,
  SET_NEWS,
  CLEAR_NEWS
} from "./newsActions";

const initialState = {
  rows: []
};

export default ( state = initialState, action ) => {
  switch (action.type) {
    case NEWS_DELETED:
      return {
        ...state,
        rows: state.rows.filter( item => item.id !== action.news_id )
      };
    case NEWS_UPDATED:
      return state.rows.map( item => {
        if( item.id === action.news.id ) return action.news;
        return item;
      });
    case ADD_NEWS:
      return {
        ...state,
        rows: [
          action.news,
          ...state.rows,
        ]
      };
    case NEWS_FETCHED:
      // busca no state se existe o mesmo balanço, que foi pego no servidor
      const index = state.rows.findIndex(item => item.id === action.news.id);
      // se existe
      if (index > -1) {
        // mapeia os itens com o mesmo id
        return {
          ...state,
          rows: state.rows.map(item => {
            // e atualiza o item, com o que foi pego no server
            if (item.id === action.news.id) return action.news;
            return item;
          })
        };
      } else {
        return {
          ...state,
          rows: [
            action.news,
            ...state.rows,
          ]
        };
      }
    case SET_NEWS:
      return action.news;
    case CLEAR_NEWS:
      return {};
    default:
      return state;
  }
};