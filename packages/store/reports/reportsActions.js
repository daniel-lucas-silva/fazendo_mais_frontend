import axios from 'axios';

export const SET_REPORTS = "SET_REPORTS";
export const ADD_REPORT = "ADD_REPORT";
export const REPORT_FETCHED = "REPORT_FETCHED";
export const REPORT_UPDATED = "REPORT_UPDATED";
export const REPORT_DELETED = "REPORT_DELETED";
export const CLEAR_REPORTS = "CLEAR_REPORTS";

export const fetchReports = (params = "") => {
  return (dispatch, getState) => {
    return new Promise((resolve, reject) => {
      const entity_id = getState().auth.user.entity.id;
      axios.get(`reports/${entity_id}${params}`)
        .then((response) => {
          dispatch({
            type: SET_REPORTS,
            reports: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          dispatch({
            type: CLEAR_REPORTS
          });
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const getReport = report_id => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`reports/get/${report_id}`)
        .then((response) => {
          dispatch({
            type: REPORT_FETCHED,
            report: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const saveReport = data => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.post(`reports`, data)
        .then((response) => {
          console.log('data ', data)
          console.log('response ', response)
          dispatch({
            type: ADD_REPORT,
            report: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const updateReport = (report_id, data) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.patch(`reports/${report_id}`, data)
        .then((response) => {
          dispatch({
            type: REPORT_UPDATED,
            report: response.data
          });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};

export const deleteReport = report_id => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.delete(`reports/${report_id}`)
        .then((response) => {
          // dispatch({
          //   type: REPORT_DELETED,
          //   report_id
          // });
          resolve(response.data);
        })
        .catch((error) => {
          console.dir(error);
          reject(error && error.response && error.response.data);
        })
    })
  }
};