import {
  ADD_REPORT,
  REPORT_DELETED,
  REPORT_FETCHED,
  REPORT_UPDATED,
  SET_REPORTS,
  CLEAR_REPORTS
} from "./reportsActions";

const initialState = {
  rows: []
};

export default ( state = initialState, action ) => {
  switch (action.type) {
    case REPORT_DELETED:
      return {
        ...state,
        rows: state.rows.filter( item => item.id !== action.report_id )
      };
    case REPORT_UPDATED:
      return state.rows.map( item => {
        if( item.id === action.report.id ) return action.report;
        return item;
      });
    case ADD_REPORT:
      return {
        ...state,
        rows: [
          action.report,
          ...state.rows,
        ]
      };
    case REPORT_FETCHED:
      // busca no state se existe o mesmo balanço, que foi pego no servidor
      const index = state.rows.findIndex(item => item.id === action.report.id);
      // se existe
      if (index > -1) {
        // mapeia os itens com o mesmo id
        return {
          ...state,
          rows: state.rows.map(item => {
            // e atualiza o item, com o que foi pego no server
            if (item.id === action.report.id) return action.report;
            return item;
          })
        };
      } else {
        return {
          ...state,
          rows: [
            action.report,
            ...state.rows,
          ]
        };
      }
    case SET_REPORTS:
      return action.reports;
    case CLEAR_REPORTS:
      return {};
    default:
      return state;
  }
};