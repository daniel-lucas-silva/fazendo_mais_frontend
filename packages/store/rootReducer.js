import { combineReducers } from 'redux';

import appReducer from "./app/appReducer";
import authReducer from "./auth/authReducer";
import categoriesReducer from "./categories/categoriesReducer";
import newsReducer from "./news/newsReducer";
import balanceReducer from "./balance/balanceReducer";
import reportsReducer from "./reports/reportsReducer";
import entitiesReducer from './entities/entitiesReducer';

export default combineReducers({
  app: appReducer,
  auth: authReducer,
  categories: categoriesReducer,
  news: newsReducer,
  balance: balanceReducer,
  reports: reportsReducer,
  entities: entitiesReducer
});